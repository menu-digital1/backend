import Vue from "vue";
import numeral from "numeral";
import locales from "numeral/locales";

numeral.locale("pt-br");

Vue.use(require("bootstrap-vue"));

Vue.filter("formatDate", function(value) {
    if (value) {
        return moment(String(value)).format("MM/DD/YYYY hh:mm");
    }
});

Vue.filter("formatTime", function(value) {
    if (value) {
        return moment(String(value)).format("HH:mm");
    }
});

Vue.filter("formatNumber", function(value, arg) {
    if (value) {
        return numeral(value).format(arg);
    }
});

Vue.filter("toCurrency", function(value) {
    if (typeof value !== "number") {
        return value;
    }
    var formatter = new Intl.NumberFormat("pt-BR", {
        style: "currency",
        currency: "BRL",
        minimumFractionDigits: 2
    });
    return formatter.format(value);
});

Vue.component("cardapio", require("../cardapio/cardapio.vue").default);
Vue.component("cardapio-categoria", require("../cardapio/cardapio_categoria.vue").default);
Vue.component("cardapio-subcategoria", require("../cardapio/cardapio_subcategoria.vue").default);
Vue.component("cardapio-produto", require("../cardapio/cardapio_produto.vue").default);
Vue.component("cardapio-variantes", require("../cardapio/variants.vue").default);
Vue.component("cardapio-variantes-categoria", require("../cardapio/variants_category.vue").default);
Vue.component("cardapio-variantes-item", require("../cardapio/variants_item.vue").default);

Vue.component("orders", require("../balcony/orders.vue").default);
Vue.component("order-item", require("../balcony/order_item.vue").default);
Vue.component("orders-finalized", require("../balcony/orders-finalized.vue").default);
Vue.component("orders-checkout", require("../balcony/orders-checkout.vue").default);
Vue.component("orders-checkout-table", require("../balcony/orders-checkout-table.vue").default);
Vue.component("orders-checkout-item", require("../balcony/orders-checkout-item.vue").default);
Vue.component("orders-checkout-comanda", require("../balcony/orders-checkout-comanda.vue").default);
Vue.component("orders-checkout-edit-item", require("../balcony/orders-checkout-edit-item.vue").default);
Vue.component("checkout-menu-button", require("../balcony/checkout_menu_button.vue").default);
Vue.component("number-spinner", require("../balcony/number_spinner.vue").default);
Vue.component("waiter-call", require("../balcony/waiter_call.vue").default);

Vue.component("comandas-check", require("../comandas/CheckComanda.vue").default);

Vue.component("orders-sales-graph", require("../orders/sales_graph.vue").default);
Vue.component("waiters-called-graph", require("../waiters/waiters_called.vue").default);


if ($("#app").length) {
    const app = new Vue({
        el: "#app"
    });
}
