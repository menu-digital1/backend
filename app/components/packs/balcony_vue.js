import Vue from 'vue';
import numeral from 'numeral';

Vue.use(require('bootstrap-vue'));

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('MM/DD/YYYY hh:mm')
  }
});

Vue.filter('formatTime', function(value) {
  if (value) {
    return moment(String(value)).format('HH:mm')
  }
});

Vue.filter('formatNumber', function(value, arg) {
  if (value) {
    return numeral(value).format(arg);
  }
});

Vue.component('cardapio', require('../cardapio/cardapio.vue'));

Vue.component('orders', require('../balcony/orders.vue'));
Vue.component('order_item', require('../balcony/order_item.vue'));
Vue.component('orders_finalized', require('../balcony/orders-finalized.vue'));
Vue.component('checkout-menu-button', require('../balcony/checkout_menu_button.vue'));

const app = new Vue({
  el: '#app',
})