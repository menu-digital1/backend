class UserPolicy < ApplicationPolicy
  def index?
    @user.role.admin?
  end

  def create?
    @user.role.admin?
  end

  def edit?
    @user.role.admin?
  end

  def destroy?
    @user.role.admin? and @record.id != @user.id
  end

  def block?
    @user.role.admin? and @record.id != @user.id
  end

  def unblock?
    @user.role.admin? and @record.id != @user.id
  end

  def change_app_settings?
    @record.role.admin?
  end
end
