class CategoryBook
  include Mongoid::Document
  store_in collection: 'categories_books'

  field :nome, type: String

  has_many :books
end
