class ItensVariante
  include Mongoid::Document
  include Mongoid::Paranoia

  field :nome, type: String
  field :valor_adicional, type: Float, default: 0.00
  field :habilitado, type: Boolean, default: true
  field :posicao, type: Integer, default: 0
  embedded_in :variante

  default_scope -> { order_by(posicao: :asc) }
end
