class CompanyData
  include Mongoid::Document

  field :nome, type: String
  field :cnpj, type: String
  field :endereco, type: String
  field :numero, type: String
  field :complemento, type: String
  field :bairro, type: String
  field :cidade, type: String
  field :uf, type: String
  field :cep, type: String
end
