class CallWaiter
  include Mongoid::Document

  field :table, type: Integer
  field :created_at, type: DateTime, default: Time.now
end