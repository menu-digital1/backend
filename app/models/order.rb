class Order
  include Mongoid::Document
  include Mongoid::Paranoia

  extend Enumerize

  enumerize :status,
            in: %i[open preparing canceled done finalized closed],
            default: :open

  field :numero, type: Integer
  # belongs_to    :comanda
  # belongs_to    :table
  field :comanda, type: Integer
  field :table, type: Integer
  field :created_at, type: DateTime, default: Time.now
  field :products, type: Array
  field :status
  field :tempo_total, type: String

  def as_json(options)
    super.as_json(options).merge({ table: self.table, late: self.late })
  end

  def late
    if (self.status.open? or self.status.preparing?)
      (Time.now - self.created_at >= 30.minutes)
    else
      false
    end
  end

  def created_at_text
    self.created_at.strftime('%d/%m/%Y %H:%M')
  end

  def order_total
    total = 0
    self.products.each do |p|
      total += p[:value] * p[:quantity]

      if not p[:variants].empty?
        p[:variants].each do |v|
          if v[:quantity]
            total += v[:value] * v[:quantity]
          else
            total += v[:value]
          end
        end
      end
    end

    total
  end
end
