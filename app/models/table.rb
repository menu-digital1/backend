class Table
  include Mongoid::Document
  include Mongoid::Paranoia

  extend Enumerize

  enumerize :status, in: %i[open used invalid waiting_payment], default: :open

  field :numero, type: Integer
  field :status

  has_many :comandas

  def table_string
    sprintf '%04d', self.numero
  end
end
