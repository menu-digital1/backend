module Imageable
  extend ActiveSupport::Concern
  include Mongoid::Paperclip

  included do
    #Imagem das categorias
    has_mongoid_attached_file :imagem
    validates_attachment_content_type :imagem,
                                      content_type: %w[
                                        image/jpg
                                        image/jpeg
                                        image/png
                                        image/gif
                                      ]

    #configuração do tema
    has_mongoid_attached_file :logo_topo
    validates_attachment_content_type :logo_topo,
                                      content_type: %w[
                                        image/jpg
                                        image/jpeg
                                        image/png
                                        image/gif
                                      ]

    has_mongoid_attached_file :favicon
    validates_attachment_content_type :favicon,
                                      content_type: %w[
                                        image/jpg
                                        image/jpeg
                                        image/png
                                        image/gif
                                      ]

    has_mongoid_attached_file :foto_home
    validates_attachment_content_type :foto_home,
                                      content_type: %w[
                                        image/jpg
                                        image/jpeg
                                        image/png
                                        image/gif
                                      ]

    has_mongoid_attached_file :foto_capa_pedidos
    validates_attachment_content_type :foto_capa_pedidos,
                                      content_type: %w[
                                        image/jpg
                                        image/jpeg
                                        image/png
                                        image/gif
                                      ]
  end
end
