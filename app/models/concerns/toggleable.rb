module Toggleable
  extend ActiveSupport::Concern

  included { field :habilitado, type: Boolean, default: false }

  module ClassMethods
    def habilitados
      where(habilitado: true)
    end
  end

  def toggle_habilitado
    self.habilitado = !self.habilitado
  end
end
