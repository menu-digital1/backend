class Variante
  include Mongoid::Document
  include Mongoid::Paranoia
  include Toggleable

  field :nome, type: String
  field :tipo, type: String
  field :habilitado, type: Boolean, default: true
  field :maxSelect, type: Integer
  field :posicao, type: Integer, default: 0
  embedded_in :pedido
  embeds_many :itens_variante

  default_scope -> { order_by(posicao: :asc) }

  def as_json(options = nil)
    super.as_json(options).merge({ tipo_text: tipo_text })
  end

  def tipo_text
    self.tipo == true ? 'Múltipla escolha' : 'Única escolha'
  end
end
