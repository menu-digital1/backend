class PrintLog
  include Mongoid::Document

  extend Enumerize

  enumerize :status, in: %i[printed print_error], default: :printed

  field :created_at, type: DateTime, default: Time.now
  field :descricao, type: String
  field :status
  field :produtos, type: Hash
  field :company_data, type: Hash
  field :table, type: Integer

  def as_json(options = nil)
    super.as_json(options).merge({ status_text: status_text })
  end
end
