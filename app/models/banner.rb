class Banner
  include Mongoid::Document

  paginates_per 8

  field :texto, type: String
  mount_uploader :imagem, BannerImageUploader
  field :data_inicio, type: Date
  field :data_fim, type: Date
  belongs_to :produto, optional: true
  belongs_to :event, optional: true

  def as_json(options)
    super.as_json(options).merge({ full_imagem: full_imagem, produto: produto, event: event })
  end

  def data_inicio_human
    self.data_inicio ? self.data_inicio.strftime('%d/%m/%Y') : nil
  end

  def data_fim_human
    self.data_fim ? self.data_fim.strftime('%d/%m/%Y') : nil
  end

  def full_imagem
    "#{$request.protocol}#{$request.host_with_port}#{self.imagem}"
  end
end
