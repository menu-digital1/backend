class Comanda
  include Mongoid::Document
  include Mongoid::Paranoia

  extend Enumerize

  enumerize :status,
            in: %i[
              open
              used
              closed
              invalid
              waiting_payment
              waiting_payment_no_comission
            ],
            default: :open

  has_many :orders

  field :codigo, type: Integer
  field :status
  belongs_to :table
end
