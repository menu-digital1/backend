class Produto
  include Mongoid::Document
  include Mongoid::Paranoia
  include Toggleable
  include ActionView::Helpers::NumberHelper

  extend Enumerize

  enumerize :tipo, in: %i[balcao cozinha], default: :balcao

  field :nome, type: String
  field :posicao, type: Integer, default: 0
  field :descricao, type: String
  field :valor_promocional, type: Float
  field :valor_final, type: Float
  field :tipo
  field :habilitado, type: Boolean, default: true

  mount_uploader :imagem, PictureUploader
  mount_uploader :gif, GifUploader

  #default_scope -> { where(habilitado: true) }
  #scope :habilidados -> { where(habilitado: true) }

  validates :nome, presence: true

  # validates :descricao,
  #   presence: true

  validates :valor_promocional, presence: true, numericality: true

  validates :valor_final,
            presence: true,
            numericality: {
              greater_than_or_equal_to: :valor_promocional,
              message: 'não pode ser menor que o valor promocional'
            }

  belongs_to :subcategorias
  embeds_many :variantes

  def as_json(options = nil)
    super.as_json(options).merge(
      { full_imagem: full_imagem, full_gif: full_gif }
    )
  end

  def full_imagem
    "#{$request.protocol}#{$request.host_with_port}#{self.imagem}"
  end

  def full_gif
    if !self.gif.blank?
      "#{$request.protocol}#{$request.host_with_port}#{self.gif}"
    else
      nil
    end
  end

  default_scope -> { order_by(posicao: :asc) }

  # before_create do |document|
  #   subcategoria = Subcategoria.find(document.subcategorias_id)
  #   if posicao_maior = subcategoria.produtos.max('posicao')
  #     document.posicao = posicao_maior + 1
  #   else
  #     document.posicao = 0
  #   end
  # end

  # after_destroy do |document|
  #   Produto.gt(posicao: document.posicao).inc(posicao: -1)
  # end

  # before_save do |document|
  #   if document.posicao_changed?
  #     reorder_produtos(document.posicao, document.posicao_was,document.subcategorias_id)
  #   end
  # end

  def reorder_produtos(new_position, old_position, subcategorias_id)
    #byebug
    subcategoria = Subcategoria.find(subcategorias_id)
    if new_position < 0 || new_position > subcategoria.produtos.max('posicao')
      raise 'Invalid position value'
    end

    if (new_position > old_position)
      Produto.gt(posicao: old_position).lte(posicao: new_position).inc(
        posicao: -1
      )
    else
      Produto.gte(posicao: new_position).lt(posicao: old_position).inc(
        posicao: 1
      )
    end
  end

  def valor_final_text
    number_to_currency self.valor_final,
                       separator: ',', delimiter: '.', unit: ''
  end

  def valor_promocional_text
    number_to_currency self.valor_promocional,
                       separator: ',', delimiter: '.', unit: ''
  end
end
