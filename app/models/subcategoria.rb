class Subcategoria
  include Mongoid::Document
  include Mongoid::Paranoia
  include Toggleable

  field :nome, type: String
  field :posicao, type: Integer
  field :habilitado, type: Boolean, default: true

  validates :nome, presence: true

  has_many :produtos, dependent: :destroy
  belongs_to :categoria

  default_scope -> { order_by(posicao: :asc) }

  # before_create do |document|
  #   if posicao_maior = Subcategoria.max('posicao')
  #     document.posicao = posicao_maior + 1
  #   else
  #     document.posicao = 0
  #   end
  # end

  # after_destroy do |document|
  #   Subcategoria.gt(posicao: document.posicao).inc(posicao: -1)
  # end

  # before_save do |document|
  #   if document.posicao_changed?
  #     reorder_subcategorias(document.posicao, document.posicao_was)
  #   end
  # end

  def reorder_subcategorias(new_position, old_position)
    if new_position < 0 || new_position > Subcategoria.max('posicao')
      raise 'Invalid position value'
    end

    if (new_position > old_position)
      Subcategoria.gt(posicao: old_position).lte(posicao: new_position).inc(
        posicao: -1
      )
    else
      Subcategoria.gte(posicao: new_position).lt(posicao: old_position).inc(
        posicao: 1
      )
    end
  end

  def produtos_habilitados
    produtos.where(habilitado: true)
  end
end
