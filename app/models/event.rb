class Event
  include Mongoid::Document
  include Mongoid::Paranoia

  field :data_inicio, type: Time
  field :data_fim, type: Time
  field :nome, type: String
  field :descricao, type: String
  mount_uploader :imagem, EventImageUploader

  def as_json(options = nil)
    super.as_json(options).merge({ full_imagem: full_imagem })
  end

  def data_inicio_human
    self.data_inicio ? self.data_inicio.strftime('%d/%m/%Y') : nil
  end

  def data_fim_human
    self.data_fim ? self.data_fim.strftime('%d/%m/%Y') : nil
  end

  def full_imagem
    "#{$request.protocol}#{$request.host_with_port}#{self.imagem}"
  end
end
