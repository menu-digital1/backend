class Categoria
  include Mongoid::Document
  include Mongoid::Paranoia
  include Toggleable

  field :nome, type: String
  field :posicao, type: Integer, default: 0
  field :habilitado, type: Boolean, default: true

  validates :nome, presence: true

  has_many :subcategorias, dependent: :destroy

  default_scope -> { order_by(posicao: :asc) }

  # before_create do |document|
  #   if posicao_maior = Categoria.max('posicao')
  #     document.posicao = posicao_maior + 1
  #   else
  #     document.posicao = 0
  #   end
  # end

  # after_destroy do |document|
  #   Categoria.gt(posicao: document.posicao).inc(posicao: -1)
  # end

  def as_json(options = nil)
    super.as_json(options).merge({ subcategorias: subcategorias })
  end

  # before_save do |document|
  #   if document.posicao_changed?
  #     reorder_categorias(document.posicao, document.posicao_was)
  #   end
  # end

  def reorder_categorias(new_position, old_position)
    if new_position < 0 || new_position > Categoria.max('posicao')
      raise 'Invalid position value'
    end

    if (new_position > old_position)
      Categoria.gt(posicao: old_position).lte(posicao: new_position).inc(
        posicao: -1
      )
    else
      Categoria.gte(posicao: new_position).lt(posicao: old_position).inc(
        posicao: 1
      )
    end
  end

  def produtos_habilitados
    produtos.where(habilitado: true)
  end

  def self.to_csv(products, options = {})
    byebug
    attributes = %w[nome descricao categoria valor_promocional valor_final]

    #CSV.generate(:col_sep => "\t", headers: true) do |csv|
    CSV.generate(options) do |csv|
      csv << attributes
      products.each do |produto|
        #csv << produto
        csv <<
          attributes.map { |attr| produto.send(attr) }
      end
    end
  end
end
