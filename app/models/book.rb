class Book
  include Mongoid::Document

  paginates_per 4

  field :titulo, type: String
  field :autor, type: String
  field :resumo, type: String
  mount_uploader :capa, BookCoverUploader
  belongs_to :category_book

  def as_json(options)
    super.as_json(options).merge({ full_capa: full_capa })
  end

  def full_capa
    "#{$request.protocol}#{$request.host_with_port}#{self.capa}"
  end
end
