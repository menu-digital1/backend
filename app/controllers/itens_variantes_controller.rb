class ItensVariantesController < ApplicationController
  before_action :set_produto, only: %i[index new edit update create destroy]
  before_action :set_variante,
                only: %i[index show new edit update create destroy]
  before_action :set_itens_variante, only: %i[show edit update destroy]

  # GET /itens_variantes
  # GET /itens_variantes.json
  def index
    @page_title =
      @produto.nome + '<small> > ' + @variante.nome + ' > Itens</small>'
    @itens_variantes = @variante.itens_variante.all
  end

  # GET /itens_variantes/1
  # GET /itens_variantes/1.json
  def show; end

  # GET /itens_variantes/new
  def new
    @page_title =
      @produto.nome + '<small> > ' + @variante.nome + ' > Novo Item</small>'
    @itens_variante = @variante.itens_variante.new
  end

  # GET /itens_variantes/1/edit
  def edit
    @page_title =
      @produto.nome + '<small> > ' + @variante.nome + ' > Editar Item</small>'
  end

  # POST /itens_variantes
  # POST /itens_variantes.json
  def create
    @itens_variante = @variante.itens_variante.new(itens_variante_params)
    @itens_variante.valor_adicional =
      itens_variante_params[:valor_adicional].gsub(',', '.').to_f

    respond_to do |format|
      if @itens_variante.save
        format.html do
          redirect_to produto_variante_itens_variantes_path(
                        @produto,
                        @variante
                      ),
                      notice: 'Item criado com sucesso.'
        end
        format.json do
          render :show, status: :created, location: @itens_variante
        end
      else
        format.html { render :new }
        format.json do
          render json: @itens_variante.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # PATCH/PUT /itens_variantes/1
  # PATCH/PUT /itens_variantes/1.json
  def update
    @itens_variante.nome = itens_variante_params[:nome]
    @itens_variante.valor_adicional =
      itens_variante_params[:valor_adicional].gsub(',', '.').to_f

    respond_to do |format|
      if @itens_variante.save
        format.html do
          redirect_to produto_variante_itens_variantes_path(
                        @produto,
                        @variante
                      ),
                      notice: 'Itens atualizado com sucesso.'
        end
        format.json { render :show, status: :ok, location: @itens_variante }
      else
        format.html { render :edit }
        format.json do
          render json: @itens_variante.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # DELETE /itens_variantes/1
  # DELETE /itens_variantes/1.json
  def destroy
    @itens_variante.destroy
    respond_to do |format|
      format.html do
        redirect_to produto_variante_itens_variantes_path(@produto, @variante),
                    notice: 'Itens removido com sucesso.'
      end
      format.json { head :no_content }
    end
  end

  def toggle_habilitado
    produto = Produto.find(params[:produto_id])
    variante = produto.variantes.find(params[:variante_id])
    item_variante = variante.itens_variante.find(params[:itens_variante_id])

    if item_variante.has_attribute?(:habilitado)
      item_variante.habilitado = !item_variante.habilitado
    else
      item_variante.habilitado = true
    end

    itens_variante =
      variante.itens_variante.where(
        :habilitado.exists => true, habilitado: true
      )
        .all
    if !itens_variante.blank?
      variante.habilitado = true
    else
      variante.habilitado = false
    end
    variante.save

    if item_variante.save
      head :ok
    else
      head :ok, status: :unprocessable_entity
    end
  end

  private

  def set_produto
    @produto = Produto.find(params[:produto_id])
  end
  # Use callbacks to share common setup or constraints between actions.
  def set_variante
    @variante = @produto.variantes.find(params[:variante_id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_itens_variante
    @itens_variante = @variante.itens_variante.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def itens_variante_params
    params.require(:itens_variante).permit(:nome, :habilitado, :valor_adicional)
  end
end
