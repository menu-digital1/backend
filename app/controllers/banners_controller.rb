class BannersController < ApplicationController
  before_action :set_banner, only: %i[show edit update destroy]

  # GET /banners
  # GET /banners.json
  def index
    @page_title = 'Banners'
    @banners = Banner.order(texto: :asc).page(params[:page])

    @banners = @banners.where(texto: /.*#{params[:q]}.*/i) if !params[:q].blank?
  end

  # GET /banners/1
  # GET /banners/1.json
  def show; end

  # GET /banners/new
  def new
    @page_title = 'Novo Banner'
    @banner = Banner.new
    @products = Produto.order_by(nome: :asc)
    @events = Event.order_by(data_inicio: :desc)
  end

  # GET /banners/1/edit
  def edit
    @page_title = 'Editar Banner'
    @products = Produto.order_by(nome: :asc)
    @events = Event.order_by(data_inicio: :desc)
  end

  # POST /banners
  # POST /banners.json
  def create
    @banner = Banner.new(banner_params)

    if (params[:banner_association_choice] == 'product')
      @banner.unset(:event_id)
    end

    if (params[:banner_association_choice] == 'event')
      @banner.unset(:produto_id)
    end

    @banner.save validate: false

    redirect_to banners_path, notice: 'Banner salvo com sucesso'
  end

  # PATCH/PUT /banners/1
  # PATCH/PUT /banners/1.json
  def update
    @banner.update(banner_params)

    if (params[:banner_association_choice] == 'product')
      @banner.unset(:event_id)
    end

    if (params[:banner_association_choice] == 'event')
      @banner.unset(:produto_id)
    end

    @banner.save validate: false

    redirect_to banners_path, notice: 'Banner salvo com sucesso'
  end

  # DELETE /banners/1
  # DELETE /banners/1.json
  def destroy
    @banner.destroy
    respond_to do |format|
      format.html do
        redirect_to banners_url, notice: 'Banner excluído com sucesso'
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_banner
    @banner = Banner.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def banner_params
    params.require(:banner).permit(
      :texto,
      :imagem,
      :data_inicio,
      :data_fim,
      :produto_id,
      :event_id
    )
  end
end
