class TablesController < ApplicationController
  before_action :set_table, only: %i[edit update destroy]

  def index
    @page_title = 'Mesas'
    @tables = Table.all
    @mesa = Table.new
  end

  def create
    table = Table.new table_params

    table.save

    redirect_to tables_path, notice: 'Mesa salva com sucesso.'
  end

  def edit
    @page_title = 'Mesas'
  end

  def update
    @mesa.update table_params

    @mesa.save

    redirect_to tables_path, notice: 'Mesa salva com sucesso.'
  end

  def destroy
    @mesa.destroy

    redirect_to tables_path, notice: 'Mesa excluída com sucesso'
  end

  private

  def set_table
    @mesa = Table.find params[:id]
  end

  def table_params
    params.require(:table).permit :numero, :status
  end
end
