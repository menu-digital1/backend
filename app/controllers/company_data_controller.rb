class CompanyDataController < ApplicationController
  before_action :set_company_data, only: %i[edit update destroy]

  def index
    @ESTADOS_BRASILEIROS = [
      %w[Acre AC],
      %w[Alagoas AL],
      %w[Amapá AP],
      %w[Amazonas AM],
      %w[Bahia BA],
      %w[Ceará CE],
      ['Distrito Federal', 'DF'],
      ['Espírito Santo', 'ES'],
      %w[Goiás GO],
      %w[Maranhão MA],
      ['Mato Grosso', 'MT'],
      ['Mato Grosso do Sul', 'MS'],
      ['Minas Gerais', 'MG'],
      %w[Pará PA],
      %w[Paraíba PB],
      %w[Paraná PR],
      %w[Pernambuco PE],
      %w[Piauí PI],
      ['Rio de Janeiro', 'RJ'],
      ['Rio Grande do Norte', 'RN'],
      ['Rio Grande do Sul', 'RS'],
      %w[Rondônia RO],
      %w[Roraima RR],
      ['Santa Catarina', 'SC'],
      ['São Paulo', 'SP'],
      %w[Sergipe SE],
      %w[Tocantins TO]
    ]

    @page_title = 'Empresa'
    @companyData = CompanyData.first_or_create
  end

  def update
    @companyData.update company_data_params

    @companyData.save validate: false

    redirect_to company_data_index_path, notice: 'Dados salvos com sucesso'
  end

  private

  def set_company_data
    @companyData = CompanyData.first
  end

  def company_data_params
    params.require(:company_data).permit(
      :nome,
      :cnpj,
      :endereco,
      :numero,
      :complemento,
      :bairro,
      :cidade,
      :uf,
      :cep
    )
  end
end
