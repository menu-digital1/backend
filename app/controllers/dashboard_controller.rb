class DashboardController < ApplicationController
  before_action :set_calendar, only: %i[index]

  def chart_pedidos
    map =
      'function() {
            day = Date(this.created_at.getFullYear(), this.created_at.getMonth(), this.created_at.getDate());
            var d = this.created_at;
            var key = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
            emit(key, {count: 1});
        }'
    reduce =
      "function(key, values) {
            var reduce = {'count' : 0};
            values.forEach(function(value){
                reduce.count += value.count;
            });
            return reduce;
        };"
    mapping = Order.map_reduce(map, reduce).out(inline: 1)
    pedidos_dia = Array.new
    array_date = date_range(1)
    array_date.each do |dia|
      #obj = mapping.where({:_id => dia}).first
      obj =
        mapping.select { |key| key['_id'] == dia.to_date }
      if obj.blank?
        pedidos_dia << [dia, 0]
      else
        pedidos_dia << [dia, obj[0]['value']['count']]
      end
    end
    # byebug
    render json: pedidos_dia
  end

  def line_chart_pedidos
    map =
      'function() {
            day = Date(this.created_at.getFullYear(), this.created_at.getMonth(), this.created_at.getDate());
            var d = this.created_at;
            var key = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
            emit(key, {count: 1});
        }'
    reduce =
      "function(key, values) {
            var reduce = {'count' : 0};
            values.forEach(function(value){
                reduce.count += value.count;
            });
            return reduce;
        };"
    date_start = params['start'].to_date
    date_end = params['end'].to_date
    #Filtrando Total de Pedidos
    pedidos_periodo =
      Order.where(
        { :created_at.gte => date_start, :created_at.lte => date_end }
      )
    pedidos_map = pedidos_periodo.map_reduce(map, reduce).out(inline: 1)
    pedidos_total = date_range(pedidos_map, params)
    #Filtrando Pedidos Entregues
    entregues_map =
      pedidos_periodo.where({ status: :finalized }).map_reduce(map, reduce).out(
        inline: 1
      )
    entregues_total = date_range(entregues_map, params)
    #Filtrando Pedidos Cancelados
    cancelados_map =
      pedidos_periodo.where({ status: :closed }).map_reduce(map, reduce).out(
        inline: 1
      )
    cancelados_total = date_range(cancelados_map, params)

    hashLinhas = [
      { "name": 'Total de Pedidos', "data": pedidos_total },
      { "name": 'Entregues', "data": entregues_total },
      { "name": 'Cancelados', "data": cancelados_total }
    ]
    #hashLinhas = [{"name":"Workout","data":{"2013-02-10":3,"2013-02-17":3,"2013-02-24":3,"2013-03-03":1}}]
    #byebug
    render json: hashLinhas
  end

  def date_range(mapping, periodo)
    date_start = periodo['start'].to_date
    date_end = periodo['end'].to_date
    array_date = (date_start..date_end).map { |date| date.strftime('%d/%m/%Y') }
    pedidos_hash = {}
    array_date.each do |dia|
      obj = mapping.select { |key| key['_id'] == dia.to_date }
      if obj.blank?
        pedidos_hash[dia] = 0
      else
        pedidos_hash[dia] = obj[0]['value']['count']
      end
    end
    return pedidos_hash
  end

  def pie_chart_pedidos
    map =
      'function() {
            var key = this.status
            emit(key, {count: 1});
        }'
    reduce =
      "function(key, values) {
            var reduce = {'count' : 0};
            values.forEach(function(value){
                reduce.count += value.count;
            });
            return reduce;
        };"
    date_start = params['start'].to_date
    date_end = params['end'].to_date
    #Filtrando Total de Pedidos
    pedidos_periodo =
      Pedido.where(
        { :created_at.gte => date_start, :created_at.lte => date_end }
      )
    pedidos_map = pedidos_periodo.map_reduce(map, reduce).out(inline: 1)
    fatias = Array.new
    pedidos_map.each do |pedido|
      fatias << [pedido['_id'].capitalize!, pedido['value']['count']]
    end

    render json: fatias
  end

  def table_pedido_itens
    map =
      'function() {
            this.pedido_itens.forEach(function(item) {
                emit(item.produto.nome, {count: item.quantidade})
            });
        }'
    reduce =
      "function(key, values) {
            var reduce = {'count' : 0};
            values.forEach(function(value){
                reduce.count += value.count;
            });
            return reduce;
        };"
    date_start = params['start'].to_date
    date_end = params['end'].to_date
    #Filtrando Itens por pedido
    pedidos_periodo =
      Pedido.where(
        { :created_at.gte => date_start, :created_at.lte => date_end }
      )
    pedidos_map = pedidos_periodo.map_reduce(map, reduce).out(inline: 1)
    itens_pedidos_hash = []
    pedidos_map.each do |item|
      itens_pedidos_hash.push(
        { nome: item['_id'], quantidade: item['value']['count'] }
      )
    end
    itens_pedidos = itens_pedidos_hash.sort_by { |hsh| hsh[:quantidade] }
    render json: itens_pedidos.reverse![0..10]
  end

  def call_waiter
    map =
      'function() {
            var key = this.table
            emit(key, {count: 1});
        }'
    reduce =
      "function(key, values) {
            var reduce = {'count' : 0};
            values.forEach(function(value){
                reduce.count += value.count;
            });
            return reduce;
        };"
    date_start = params['start'].to_date
    date_end = params['end'].to_date
    calls =
      CallWaiter.where(
        { :created_at.gte => date_start, :created_at.lte => date_end }
      )
    calls_map = calls.map_reduce(map, reduce).out(inline: 1)
    call_waiter = []
    calls_map.each do |table|
      item = ["Mesa #{table[:_id].to_i}", table[:value][:count].to_i]
      call_waiter.push(item)
    end
    render json: call_waiter
  end

  def historico_pedido_itens
    date_start = params['start'].to_date
    date_end = params['end'].to_date
    #Filtrando Itens por pedido
    pedidos_periodo =
      Pedido.where(
        { :created_at.gte => date_start, :created_at.lte => date_end }
      )

    render json: historico_pedido(pedidos_periodo)
  end

  def set_calendar
    @calendario_meses = {
      'Jan' => 1,
      'Fev' => 2,
      'Mar' => 3,
      'Abr' => 4,
      'Mai' => 5,
      'Jun' => 6,
      'Jul' => 7,
      'Ago' => 8,
      'Set' => 9,
      'Out' => 10,
      'Nov' => 11,
      'Dez' => 12
    }

    @calendario_anos = []
    2_016..Time.now.strftime('%Y').to_i.each do |i|
      @calendario_anos << i
    end
  end
end
