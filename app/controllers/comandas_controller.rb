class ComandasController < ApplicationController
  before_action :set_comanda, only: [:show, :edit, :update, :destroy, :toggle_active]

  layout 'balcony', only: [:check_comanda]

  # GET /comandas
  # GET /comandas.json
  def index
    @comandas = Comanda.all
    @page_title = 'Comandas'
  end

  # GET /comandas/1
  # GET /comandas/1.json
  def show
  end

  # GET /comandas/new
  def new
	  @comanda = Comanda.new
  end

  # GET /comandas/1/edit
  def edit
    @page_title = 'Comandas'
    @tables = Table.all
  end

  # POST /comandas
  # POST /comandas.json
  def create
    (params[:start]..params[:end]).each do |code|
      comanda = Comanda.where(:codigo => code).first_or_create

      comanda.codigo = code

      comanda.save validate: false
	  end

	  redirect_to comandas_path, notice: 'Comandas criadas com sucesso.'
  end

  # PATCH/PUT /comandas/1
  # PATCH/PUT /comandas/1.json
  def update
    if params[:comanda][:table_id] == ''
      @comanda.unset(:table_id)
    end

    @comanda.update comanda_params

    @comanda.save validate: false
    
    redirect_to comandas_path, notice: 'Comanda salva com sucesso.'
  end

  # DELETE /comandas/1
  # DELETE /comandas/1.json
  def destroy
    @comanda.destroy
    
    redirect_to comandas_path, notice: 'Comanda excluída com sucesso.'
  end

  def toggle_active
    @comanda.ativo = !@comanda.ativo
    @comanda.save
  end

  def check_comanda
    
  end

  def check_comanda_action
    comanda = Comanda.where(codigo: params[:comanda]).first

    if not comanda
      render json: 'not_exists'

      return
    end

    orders = Order.where(comanda: params[:comanda], :status.ne => :closed)

    if orders.empty?
      render json: 'ok'
    else
      render json: 'not_ok'
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_comanda
	  @comanda = Comanda.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def comanda_params
	  params.require(:comanda).permit(:codigo, :table_id, :status)
  end
end
