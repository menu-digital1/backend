class CategoriesBooksController < ApplicationController
  before_action :set_category, only: %i[edit update destroy]

  def index
    @page_title = 'Categorias <small>Livros</small>'

    @categories = CategoryBook.all
    @category = CategoryBook.new
  end

  def create
    @category = CategoryBook.new category_params

    @category.save

    redirect_to categories_books_path, notice: 'Categoria salva com sucesso'
  end

  def edit
    @page_title = 'Categorias <small>Livros</small>'
  end

  def update
    @category.update category_params
    @category.save

    redirect_to categories_books_path, notice: 'Categoria salva com sucesso'
  end

  def destroy
    @category.destroy

    redirect_to categories_books_path, notice: 'Categoria excluída com sucesso'
  end

  private

  def set_category
    @category = CategoryBook.find params[:id]
  end

  def category_params
    params.require(:category_book).permit :nome
  end
end
