class ProdutosController < ApplicationController
  before_action :set_produto, only: %i[show edit update destroy]

  skip_before_action :verify_authenticity_token, only: %i[destroy]

  # GET /produtos
  # GET /produtos.json
  def index
    @page_title = 'Produtos'
    @produtos = Produto.all
  end

  # GET /produtos/1
  # GET /produtos/1.json
  def show; end

  # GET /produtos/new
  def new
    @page_title = 'Produto <small>Novo</small>'
    @produto = Produto.new
    @produto.subcategorias_id = params[:subcategory]
  end

  # GET /produtos/1/edit
  def edit
    @page_title = 'Produto <small>Editar</small>'
  end

  # POST /produtos
  # POST /produtos.json
  def create
    products =
      Produto.where(subcategorias_id: params[:produto][:subcategorias_id])
        .order_by(posicao: :asc)

    products.each do |p|
      p.posicao = p.posicao + 1
      p.save validate: false
    end

    @produto = Produto.new produto_params

    @produto.valor_final = parse_currency params[:produto][:valor_final]
    @produto.valor_promocional =
      parse_currency params[:produto][:valor_promocional]

    @produto.save validate: false

    redirect_to cardapio_index_path, notice: 'Produto salvo com sucesso.'
  end

  def parse_currency(value)
    value.gsub('.', '').gsub(',', '.').to_f
  end

  # PATCH/PUT /produtos/1
  # PATCH/PUT /produtos/1.json
  def update
    @produto.update produto_params

    @produto.valor_final = parse_currency params[:produto][:valor_final]
    @produto.valor_promocional =
      parse_currency params[:produto][:valor_promocional]

    @produto.save validate: false

    redirect_to cardapio_index_path, notice: 'Produto salvo com sucesso.'
  end

  # DELETE /produtos/1
  # DELETE /produtos/1.json
  def destroy
    @produto.destroy
    # respond_to do |format|
    #     format.html { redirect_to produtos_url, notice: 'Produto removido com sucesso.' }
    #     format.json { head :no_content }
    # end
  end

  #Habilitar e desabilitar produto
  def toggle_habilitado
    @produto = Produto.find(params[:produto_id])
    @produto.toggle_habilitado

    if @produto.save
      head :ok
    else
      head :ok, status: :unprocessable_entity
    end
  end

  def add_categoria
    categoria = Categoria.new params.permit(:nome)
    categoria.save
    render json: categoria
  end

  #Ajustar posicao da categoria
  def update_posicao
    @produto = Produto.find(params[:produto_id])
    @produto.posicao = params[:posicao]

    if @produto.save
      head :ok
    else
      head :ok, status: :unprocessable_entity
    end
  end

  private

  def set_categoria
    #@categoria = Categoria.find(params[:categoria_id])
    #@categoria = Categoria.find(params[:produto][:categoria_id])
    @categoria = @subcategoria.categoria_id
  end

  def set_subcategoria
    if params[:produto].nil?
      @subcategoria = Subcategoria.find(@produto.subcategorias.id)
      #byebug
    else
      @subcategoria = Subcategoria.find(params[:produto][:subcategoria])
      #byebug
    end
  end

  def set_produto
    @produto = Produto.find(params[:id])
    #byebug
  end

  def produto_params
    params.require(:produto).permit(
      :nome,
      :descricao,
      :valor_promocional,
      :valor_final,
      :imagem,
      :gif,
      :tipo,
      :subcategorias_id
    )
  end
end
