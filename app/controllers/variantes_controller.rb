class VariantesController < ApplicationController
  before_action :set_produto, only: %i[index new edit update create destroy]
  before_action :set_variante, only: %i[show edit update destroy]

  # GET /variantes
  # GET /variantes.json
  def index
    @page_title = @produto.nome + ' <small>Variantes</small>'
    @variantes = @produto.variantes.all
  end

  # GET /variantes/1
  # GET /variantes/1.json
  def show; end

  # GET /variantes/new
  def new
    @page_title = @produto.nome + ' <small>Nova Variante</small>'
    @variante = @produto.variantes.new
  end

  # GET /variantes/1/edit
  def edit
    @page_title = @produto.nome + ' <small>Editar Variante</small>'
  end

  # POST /variantes
  # POST /variantes.json
  def create
    @variante = @produto.variantes.new(variante_params)

    respond_to do |format|
      if @variante.save
        format.html do
          redirect_to produto_variantes_path(@produto),
                      notice: 'Variante criada com sucesso.'
        end
        format.json { render :show, status: :created, location: @variante }
      else
        format.html { render :new }
        format.json do
          render json: @variante.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # PATCH/PUT /variantes/1
  # PATCH/PUT /variantes/1.json
  def update
    respond_to do |format|
      if @variante.update(variante_params)
        format.html do
          redirect_to produto_variantes_path(@produto),
                      notice: 'Variante atualizada com sucesso.'
        end
        format.json { render :show, status: :ok, location: @variante }
      else
        format.html { render :edit }
        format.json do
          render json: @variante.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # DELETE /variantes/1
  # DELETE /variantes/1.json
  def destroy
    @variante.destroy
    respond_to do |format|
      format.html do
        redirect_to produto_variantes_path(@produto),
                    notice: 'Variante removida com sucesso.'
      end
      format.json { head :no_content }
    end
  end

  def toggle_habilitado
    @variante.toggle_habilitado

    if @variante.save
      render nothing: true
    else
      render nothing: true, status: :unprocessable_entity
    end
  end

  #Habilitar e desabilitar produto
  def toggle_habilitado
    #byebug
    @produto = Produto.find(params[:produto_id])
    @variante = @produto.variantes.find(params[:variante_id])
    @variante.toggle_habilitado

    if @variante.save
      head :ok
    else
      head :ok, status: :unprocessable_entity
    end
  end

  private

  def set_produto
    @produto = Produto.find(params[:produto_id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_variante
    @variante = @produto.variantes.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def variante_params
    params.require(:variante).permit(:nome, :tipo, :maxSelect)
  end
end
