class CardapioController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    @page_title = 'Cardápio'
  end

  def fetch_categories
    render json: Categoria.order_by(posicao: :asc)
  end

  def add_category
    category = Categoria.new

    category.nome = params[:name]
    category.posicao = 0

    category.save validate: false

    render json: category
  end

  def update_category
    category = Categoria.find params[:id]

    category.nome = params[:name]

    category.save validate: false
  end

  def delete_category
    category = Categoria.find params[:id]

    category.destroy
  end

  def reorder_categories
    params[:categories].each do |c|
      category = Categoria.find c['_id']

      category.posicao = c[:posicao]

      category.save validate: false
    end
  end

  def toggle_category
    category = Categoria.find params[:id]

    category.habilitado = params[:active]

    category.save validate: false
  end

  def add_subcategory
    subcategory = Subcategoria.new

    subcategory.nome = params[:name]
    subcategory.posicao = 0
    subcategory.categoria_id = params[:category_id]

    subcategory.save validate: false

    render json: subcategory
  end

  def update_subcategory
    subcategory = Subcategoria.find params[:id]

    subcategory.nome = params[:name]

    subcategory.save validate: false
  end

  def delete_subcategory
    subcategory = Subcategoria.find params[:id]

    subcategory.destroy
  end

  def reorder_subcategories
    params[:subcategories].each do |s|
      subcategory = Subcategoria.find s['_id']

      subcategory.posicao = s[:posicao]

      subcategory.save validate: false
    end
  end

  def toggle_subcategory
    subcategory = Subcategoria.find params[:id]

    subcategory.habilitado = params[:active]

    subcategory.save validate: false
  end

  def fetch_produtos
    $request = request

    render json:
             Produto.where(subcategorias_id: params[:subcategory]).order_by(
               posicao: :asc
             )
  end

  def toggle_product
    product = Produto.find params[:id]

    product.habilitado = params[:active]

    product.save validate: false
  end

  def reorder_products
    params[:products].each do |s|
      product = Produto.find s['_id']

      product.posicao = s[:posicao]

      product.save validate: false
    end
  end

  def toggle_variant
    product = Produto.find params[:product_id]
    variant = product.variantes.find params[:id]

    variant.habilitado = params[:active]

    variant.save validate: false
  end

  def reorder_variants
    params[:variants].each do |v|
      product = Produto.find params[:product_id]
      variant = product.variantes.find v['_id']

      variant.posicao = v[:posicao]

      variant.save validate: false
    end
  end

  def update_variant
    product = Produto.find params[:product_id]
    variant = product.variantes.find params[:id]

    variant.nome = params[:nome]
    variant.tipo = params[:tipo]
    variant.maxSelect = params[:maxSelect]

    variant.save validate: false
  end

  def create_variant
    product = Produto.find params[:product_id]
    variant = product.variantes.new

    variant.nome = params[:nome]
    variant.tipo = params[:tipo]
    variant.maxSelect = params[:maxSelect]

    variant.save validate: false

    render json: variant
  end

  def delete_variant
    product = Produto.find params[:product]
    variant = product.variantes.find params[:id]

    variant.destroy
  end

  def toggle_variant_item
    product = Produto.find params[:product_id]
    variant = product.variantes.find params[:variant_id]
    item = variant.itens_variante.find params[:id]

    item.habilitado = params[:active]

    item.save validate: false
  end

  def reorder_variants_items
    params[:items].each do |i|
      product = Produto.find params[:product_id]
      variant = product.variantes.find params[:variant_id]
      item = variant.itens_variante.find i['_id']

      item.posicao = i[:posicao]

      item.save validate: false
    end
  end

  def update_variant_item
    product = Produto.find params[:product_id]
    variant = product.variantes.find params[:variant_id]
    item = variant.itens_variante.find params[:id]

    item.nome = params[:nome]
    item.valor_adicional = params[:valor_adicional]

    item.save validate: false
  end

  def create_variant_item
    product = Produto.find params[:product_id]
    variant = product.variantes.find params[:variant_id]
    item = variant.itens_variante.new

    item.nome = params[:nome]
    item.valor_adicional = params[:valor_adicional]

    item.save validate: false

    render json: item
  end

  def delete_variant_item
    product = Produto.find params[:product]
    variant = product.variantes.find params[:variant]
    item = variant.itens_variante.find params[:id]

    item.destroy
  end
end
