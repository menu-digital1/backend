class BalconyController < ApplicationController
  require 'pusher'

  layout 'balcony'

  skip_before_action :verify_authenticity_token
  skip_before_action :authenticate_user!

  def index; end

  def fetch_orders
    render json: Order.where(:status.ne => :finalized).order_by(created_at: :asc)
  end

  def update_order
    order = Order.find params[:id]

    order.status = params[:status]

    if order.status.canceled? or order.status.done? or order.status.finalized?
      order.tempo_total = params[:total_time]
    end

    order.save validate: false

    Pusher.trigger('orders-channel', 'update-orders', { message: true })
  end

  def finalized; end

  def fetch_finalized
    render json:
             Order.where(
               status: :finalized,
               created_at:
                 (
                   DateTime.now.in_time_zone(Time.zone)
                     .beginning_of_day..DateTime.now.in_time_zone(Time.zone)
                     .end_of_day
                 )
             )
               .order_by(created_at: :desc)
  end

  def checkout; end

  def fetch_checkout
    orders = Order.where(:status.ne => :closed)

    buffer = {}
    orders.each do |o|
      table_number = o.table

      comanda = Comanda.where(codigo: o.comanda).first

      buffer[table_number] = {} if not buffer[table_number]
      buffer[table_number][comanda.codigo] = {} if not buffer[table_number][comanda.codigo]

      buffer[table_number][comanda.codigo][:number] = comanda.codigo
      buffer[table_number][comanda.codigo][:status] = comanda.status

      buffer[table_number][comanda.codigo][:products] = [] if not buffer[table_number][comanda.codigo][:products]

      o.products.each do |p|
        product = {
          id: p[:id],
          name: p[:name],
          quantity: p[:quantity],
          value: p[:value],
          pedido: o.numero,
          order_id: o.id,
          variants: p[:variants],
        }
        
        buffer[table_number][comanda.codigo][:products] << product
      end
    end

    render json: buffer.empty? ? nil : buffer

    # comandas = Comanda.where(:status.ne => :closed)

    # buffer = {}
    # comandas.each do |c|
    #   orders = Order.where(comanda: c.codigo)

    #   orders.each do |o|
    #     if o.status != :closed
    #       comanda_number = c.codigo
    #       table_number = o.table

    #       o.products.each do |p|
    #         buffer[table_number] = {} if not buffer[table_number]

    #         if not buffer[table_number][comanda_number]
    #           buffer[table_number][comanda_number] = {}
    #         end

    #         buffer[table_number][comanda_number]['comanda'] = c

    #         if not buffer[table_number][comanda_number]['products']
    #           buffer[table_number][comanda_number]['products'] = {}
    #         end

    #         if not buffer[table_number][comanda_number]['products'][p[:id]]
    #           buffer[table_number][comanda_number]['products'][p[:id]] = {}
    #         end

    #         if not buffer[table_number][comanda_number]['products'][p[:id]][
    #            :quantity
    #          ]
    #           buffer[table_number][comanda_number]['products'][p[:id]][
    #             :quantity
    #           ] =
    #             0
    #         end

    #         if not buffer[table_number][comanda_number]['products'][p[:id]][
    #            :value
    #          ]
    #           buffer[table_number][comanda_number]['products'][p[:id]][:value] =
    #             0
    #         end

    #         buffer[table_number][comanda_number]['products'][p[:id]][:checked] =
    #           true
    #         buffer[table_number][comanda_number]['products'][p[:id]][:id] =
    #           p[:id]
    #         buffer[table_number][comanda_number]['products'][p[:id]][
    #           :order_id
    #         ] =
    #           o.id
    #         buffer[table_number][comanda_number]['products'][p[:id]][
    #           :quantity
    #         ] +=
    #           p[:quantity]
    #         buffer[table_number][comanda_number]['products'][p[:id]][:name] =
    #           p[:name]

    #         final_value = p[:value] * p[:quantity]
    #         if not p[:variants].empty?
    #           p[:variants].each do |v|
    #             if v[:quantity]
    #               final_value += v[:value] * v[:quantity]
    #             else
    #               final_value += v[:value]
    #             end
    #           end
    #         end

    #         buffer[table_number][comanda_number]['products'][p[:id]][:value] +=
    #           final_value
    #       end
    #     end
    #   end
    # end

    # render json: buffer.empty? ? nil : buffer
  end

  def fetch_checkout_waiting
    render json: Table.where(status: :waiting_payment).count
  end

  def close_table
    table = Table.where(numero: params[:table]).first
    comandas = Comanda.where(:codigo.in => params[:comandas])

    buffer = {}
    comandas.each do |c|
      orders = Order.where(comanda: c.codigo)

      orders.each do |o|
        comanda_number = c.codigo
        order_table = o.table

        if o.status != :closed
          o.products.each do |p|
            buffer[comanda_number] = {} if not buffer[comanda_number]

            buffer[comanda_number]['comanda'] = c

            if not buffer[comanda_number]['products']
              buffer[comanda_number]['products'] = {}
            end

            if not buffer[comanda_number]['products'][order_table]
              buffer[comanda_number]['products'][order_table] = {}
            end

            if not buffer[comanda_number]['products'][p[:id]]
              buffer[comanda_number]['products'][p[:id]] = {}
            end

            if not buffer[comanda_number]['products'][p[:id]][order_table]
              buffer[comanda_number]['products'][p[:id]][order_table] = {}
            end

            if not buffer[comanda_number]['products'][p[:id]][order_table][
               :quantity
             ]
              buffer[comanda_number]['products'][p[:id]][order_table][
                :quantity
              ] =
                0
            end

            if not buffer[comanda_number]['products'][p[:id]][order_table][
               :value
             ]
              buffer[comanda_number]['products'][p[:id]][order_table][:value] =
                0
            end

            buffer[comanda_number]['products'][p[:id]][order_table][
              :quantity
            ] +=
              p[:quantity]
            buffer[comanda_number]['products'][p[:id]][order_table][:name] =
              p[:name]
            buffer[comanda_number]['products'][p[:id]][order_table][:table] =
              order_table

            final_value = p[:value] * p[:quantity]
            if not p[:variants].empty?
              p[:variants].each do |v|
                if v[:quantity]
                  final_value += v[:value] * v[:quantity]
                else
                  final_value += v[:value]
                end
              end
            end

            buffer[comanda_number]['products'][p[:id]][order_table][:value] +=
              final_value
          end
        end
      end
    end

    result = {}
    buffer.each do |comanda, items|
      result[comanda] = {} if not result[comanda]

      result[comanda]['comanda'] = items['comanda']

      items['products'].each do |productId, tables|
        tables.each do |table, product|
          result[comanda] = [] if not result[comanda]

          result[comanda]['products'] = [] if not result[comanda]['products']

          result[comanda]['products'].push product
        end
      end
    end

    comandas.each do |c|
      orders = Order.where(comanda: c.codigo)

      orders.each do |o|
        if o.status != :closed
          o.status = :closed
          o.save validate: false
        end
      end

      c.status = :closed
      c.unset(:table)
      c.save validate: false
    end

    table.status = :open
    table.save validate: false

    Pusher.trigger(
      'orders-channel',
      'update-closed-accounts',
      { message: true }
    )

    Pusher.trigger(
      'orders-channel',
      'print-checkout',
      { company_data: CompanyData.first, table: table, orders: result }
    )

    render json: true
  end

  def close_comanda
    comanda = Comanda.where(codigo: params[:comanda]).first

    buffer = {}
    tables_array = []

    orders = Order.where(comanda: comanda.codigo)

    orders.each do |o|
      comanda_number = comanda.codigo
      order_table = o.table

      if o.status != :closed
        tables_array.push order_table

        o.products.each do |p|
          buffer[comanda_number] = {} if not buffer[comanda_number]

          if not buffer[comanda_number][:comanda]
            buffer[comanda_number][:comanda] = {}
          end

          buffer[comanda_number][:comanda][:codigo] = comanda.codigo
          buffer[comanda_number][:comanda][:status] = comanda.status

          if not buffer[comanda_number][:products]
            buffer[comanda_number][:products] = {}
          end

          if not buffer[comanda_number][:products][order_table]
            buffer[comanda_number][:products][order_table] = {}
          end

          if not buffer[comanda_number][:products][p[:id]]
            buffer[comanda_number][:products][p[:id]] = {}
          end

          if not buffer[comanda_number][:products][p[:id]][order_table]
            buffer[comanda_number][:products][p[:id]][order_table] = {}
          end

          if not buffer[comanda_number][:products][p[:id]][order_table][
             :quantity
           ]
            buffer[comanda_number][:products][p[:id]][order_table][:quantity] =
              0
          end

          if not buffer[comanda_number][:products][p[:id]][order_table][:value]
            buffer[comanda_number][:products][p[:id]][order_table][:value] = 0
          end

          buffer[comanda_number][:products][p[:id]][order_table][:quantity] +=
            p[:quantity]
          buffer[comanda_number][:products][p[:id]][order_table][:name] =
            p[:name]
          buffer[comanda_number][:products][p[:id]][order_table][:table] =
            order_table

          final_value = p[:value] * p[:quantity]
          if not p[:variants].empty?
            p[:variants].each do |v|
              if v[:quantity]
                final_value += v[:value] * v[:quantity]
              else
                final_value += v[:value]
              end
            end
          end

          buffer[comanda_number][:products][p[:id]][order_table][:value] +=
            final_value
        end
      end
    end

    result = {}
    buffer.each do |comanda, items|
      result[comanda] = {} if not result[comanda]

      result[comanda][:comanda] = items[:comanda]

      items[:products].each do |productId, tables|
        tables.each do |table, product|
          result[comanda] = [] if not result[comanda]

          result[comanda][:products] = [] if not result[comanda][:products]

          result[comanda][:products].push product
        end
      end
    end

    orders.each do |o|
      if o.status != :closed
        o.status = :closed
        o.save validate: false
      end
    end

    comanda.status = :closed
    comanda.unset(:table_id)
    comanda.save validate: false

    tables = Table.where(:numero.in => tables_array)
    tables.each do |t|
      orders_table = Order.where(table: t.numero, :status.ne => :closed)

      if orders_table.empty?
        t.status = :open
        t.save validate: false
      end
    end

    Pusher.trigger(
      'orders-channel',
      'update-closed-accounts',
      { message: true }
    )

    Pusher.trigger(
      'orders-channel',
      'print-checkout',
      { company_data: CompanyData.first, orders: result }
    )

    render json: true
  end

  def print
    Pusher.trigger(
      'orders-channel',
      'print-checkout',
      { message: true, print: params }
    )
  end

  def update_product_quantity
    order = Order.find params[:order_id]

    product = order.products.find { |p| p[:id] == params[:product_id] }

    product[:quantity] = params[:quantity].to_i

    order.save
  end

  def delete_order_product
    order = Order.find params[:order]

    order.products.delete_if { |p| p[:id] == params[:product] }

    order.save

    if order.products.empty?
      comanda = Comanda.where(codigo: order.comanda).first

      comanda.status = :closed
      comanda.unset :table_id
      comanda.save validate: false

      table = Table.where(numero: order.table).first

      if table.comandas.empty?
        table.status = :open
        table.save validate: false
      end

      order.destroy

      Pusher.trigger(
        'orders-channel',
        'update-closed-accounts',
        { message: true }
      )
    end
  end

  def products_update
    params[:data].each do |p|
      order = Order.find p[:order_id]
      
      if p[:delete] == true
        order.products.delete_if { |pr| pr[:id] == p[:id] }

        order.save

        orders_comandas = Order.where(comanda: order.comanda)

        empty_products = true
        orders_comandas.each do |oc|
          if not oc.products.empty?
            empty_products = false
          end
        end
    
        if empty_products
          # comanda = Comanda.where(codigo: order.comanda).first
    
          # comanda.status = :closed
          # comanda.unset :table_id
          # comanda.save validate: false
    
          # table = Table.where(numero: order.table).first
    
          # if table.comandas.empty?
          #   table.status = :open
          #   table.save validate: false
          # end
    
          order.destroy
        end
      else
        product = order.products.find { |pr| pr[:id] == p[:id] }

        product[:quantity] = p[:quantity].to_i
    
        order.save
      end
    end

    Pusher.trigger(
      'orders-channel',
      'update-closed-accounts',
      { message: true }
    )
  end
end
