class EventsController < ApplicationController
  before_action :set_event, only: %i[show edit update destroy]

  def index
    @page_title = 'Eventos'
    @events = Event.order(data_inicio: :desc).page(params[:page])

    if !params[:q].blank?
      @events =
        @events.any_of(
          { nome: /.*#{params[:q]}.*/i },
          { descricao: /.*#{params[:q]}.*/i }
        )
    end

    if !params[:data_inicio].blank? and !params[:data_fim].blank?
      @events =
        @events.where(
          {
            :data_inicio.lte => params[:data_inicio],
            :data_fim.gte => params[:data_fim]
          }
        )
    end
  end

  def new
    @page_title = 'Eventos <small>Novo</small>'
    @event = Event.new
  end

  def create
    @event = Event.new(event_params)

    @event.data_inicio = @event.data_inicio.change hour: 0, min: 0, sec: 0
    @event.data_fim = @event.data_fim.change hour: 23, min: 59, sec: 59

    @event.save validate: false

    redirect_to events_path, notice: 'Evento salvo com sucesso'
  end

  def edit
    @page_title = 'Eventos <small>Editar</small>'
  end

  def update
    @event.update(event_params)

    @event.data_inicio = @event.data_inicio.change({hour: 0, min: 0, sec: 0})
    @event.data_fim = @event.data_fim.change({hour: 23, min: 59, sec: 59})

    @event.save validate: false

    redirect_to events_path, notice: 'Evento salvo com sucesso'
  end

  def destroy
    @event.destroy

    redirect_to events_path, notice: 'Evento excluído com sucesso'
  end

  private

  def set_event
    @event = Event.find params[:id]
  end

  def event_params
    params.require(:event).permit(
      :nome,
      :data_inicio,
      :data_fim,
      :imagem,
      :descricao
    )
  end
end
