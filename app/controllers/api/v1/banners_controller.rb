class Api::V1::BannersController < ActionController::Base
  skip_before_action :verify_authenticity_token

  def fetch
    $request = request

    banners =
      Banner.any_of(
        { :data_inicio.lte => Date.today, :data_fim.gte => Date.today },
        { data_inicio: nil, data_fim: nil }
      )

    render json: banners
  end
end
