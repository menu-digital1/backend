class Api::V1::PedidoController < ActionController::Base
  require 'pusher'

  skip_before_action :verify_authenticity_token

  def create
    error = false
    error_code = ''
    invalid_comandas = []

    table = Table.where(numero: params[:table]).first

    if table.status.invalid?
      error = true
      error_code = 'invalid_table'
    else
      params[:order].each do |o|
        comanda = Comanda.where(codigo: o[:number]).first

        if !comanda
          invalid_comandas << sprintf('%02d', comanda.codigo)
        else
          if !comanda.table
            invalid_comandas << sprintf('%02d', comanda.codigo)
          else
            if comanda.table.numero != params[:table].to_i
              invalid_comandas << sprintf('%02d', comanda.codigo)
            else
              if !comanda.status.open? and !comanda.status.used?
                invalid_comandas << sprintf('%02d', comanda.codigo)
              end
            end
          end
        end
      end

      if !invalid_comandas.empty?
        error = true
        error_code = 'invalid_comandas'
      end
    end

    if !error
      params[:order].each do |o|
        comanda = Comanda.where(codigo: o[:number]).first

        order = Order.new

        last_order = Order.order_by(numero: :desc).first

        if last_order.nil?
          order.numero = 1
        else
          order.numero = last_order.numero + 1
        end

        order.comanda = comanda.codigo
        order.table = table.numero
        order.created_at = Time.now

        products = []
        o[:products].each do |p|
          variants = []

          if p[:variants]
            p[:variants].each do |v|
              variants <<
                {
                  id: v[:id],
                  value: v[:value],
                  name: v[:name],
                  quantity: v[:quantity],
                  selected: v[:selected]
                }
            end
          end

          products <<
            {
              id: p[:id],
              value: p[:value],
              name: p[:name],
              quantity: p[:quantity],
              observation: p[:observation],
              tipo: p[:tipo],
              variants: variants
            }
        end

        order.products = products

        order.save if !products.empty?
      end

      Pusher.trigger('orders-channel', 'update-orders', { message: true })
    end

    render json: {
             error: error,
             error_code: error_code,
             invalid_comandas: invalid_comandas
           }
  end

  def orders
    orders = Order.where({ :comanda.in => params[:comandas], :status.ne => :closed })

    buffer = {}
    orders.each do |o|
      comanda = o.comanda
      table = o.table

      o.products.each do |p|
        buffer[comanda] = {} if not buffer[comanda]

        buffer[comanda][p[:id]] = {} if not buffer[comanda][p[:id]]

        if not buffer[comanda][p[:id]][table]
          buffer[comanda][p[:id]][table] = {}
        end

        if not buffer[comanda][p[:id]][table][:quantity]
          buffer[comanda][p[:id]][table][:quantity] = 0
        end

        if not buffer[comanda][p[:id]][table][:value]
          buffer[comanda][p[:id]][table][:value] = 0
        end

        buffer[comanda][p[:id]][table][:quantity] += p[:quantity]
        buffer[comanda][p[:id]][table][:name] = p[:name]
        buffer[comanda][p[:id]][table][:table] = table

        final_value = p[:value] * p[:quantity]
        if not p[:variants].empty?
          p[:variants].each do |v|
            if v[:quantity]
              final_value += v[:value] * v[:quantity]
            else
              final_value += v[:value]
            end
          end
        end

        buffer[comanda][p[:id]][table][:value] += final_value

        buffer[comanda][p[:id]][table][:status] = o.status
      end
    end

    result = {}
    buffer.each do |comanda, products|
      products.each do |productId, tables|
        tables.each do |table, product|
          result[comanda] = [] if not result[comanda]

          result[comanda].push product
        end
      end
    end

    render json: result.to_a
  end
end
