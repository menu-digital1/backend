class Api::V1::CheckoutController < ActionController::Base
  skip_before_action :verify_authenticity_token
  require 'pusher'

  def ordersby_comanda
    comanda = Comanda.where(codigo: params[:commandaNumber]).first
    orders = Order.where(comanda_id: comanda.id, status: :open).all

    itens_comanda = []
    total = 0

    orders.each do |order|
      order.products.each do |product|
        soma_variantes = 0

        product['variants'].each do |variant|
          soma_variantes =
            soma_variantes + (variant['value'] * variant['quantity'])
        end

        produto = {
          quantity: product['quantity'],
          nome: product['name'],
          value: product['value'] * product['quantity'] + soma_variantes,
          variants: product['variants']
        }
        #byebug
        total = total + produto[:value]
        itens_comanda.push(produto)
      end
    end

    order_sumary = { products: itens_comanda, total: total }

    #byebug
    render json: order_sumary.to_json
  end

  def pedir_conta
    comandas = params[:comandas]

    comandas.each do |c|
      comanda = Comanda.where(codigo: c[:number]).first
      comanda.status =
        if c[:payComission] == true
          :waiting_payment
        else
          :waiting_payment_no_comission
        end
      comanda.save
    end

    table = Table.where(numero: params[:table]).first
    table.status = :waiting_payment
    table.save

    Pusher.trigger(
      'orders-channel',
      'close-account',
      { message: true, table: params[:table] }
    )
    Pusher.trigger(
      'orders-channel',
      'update-closed-accounts',
      { message: true }
    )

    render json: true
  end

  def print
    Pusher.trigger(
      'orders-channel',
      'print-checkout',
      { message: true, print: params }
    )
    render json: true
  end
end
