class Api::V1::BooksController < ActionController::Base
  skip_before_action :verify_authenticity_token

  def categories
    render json:
             Book.all.group_by { |b| b.category_book.nome }.group_by { |c|
               c[0][0]
             }.to_a
  end

  def fetch
    $request = request

    books = Book.order(titulo: :asc)

    if !params[:category].blank?
      categories_ids = CategoryBook.where(nome: params[:category]).pluck(:id)
      books = books.where(:category_book_id.in => categories_ids)
    end

    if !params[:search].blank?
      categories_ids =
        CategoryBook.where(nome: /.*#{params[:search]}.*/i).pluck(:id)
      books =
        books.any_of(
          { titulo: /.*#{params[:search]}.*/i },
          { autor: /.*#{params[:search]}.*/i },
          { resumo: /.*#{params[:search]}.*/i },
          { :category_book_id.in => categories_ids }
        )
    end

    render json: books
  end
end
