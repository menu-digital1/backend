class Api::V1::EventsController < ActionController::Base
	skip_before_action :verify_authenticity_token

	def fetch
		$request = request

		events = Event.order_by(data_inicio: :desc)
		
		case params[:filter]
			when 'next'
				start_date = DateTime.now.change({hour: 0, min: 0, sec: 0})
				end_date = 15.days.from_now.change({hour: 23, min: 59, sec: 59})
			when 'today'
				start_date = DateTime.now.change({hour: 0, min: 0, sec: 0})
				end_date = DateTime.now.change({hour: 23, min: 59, sec: 59})
			when 'week'
				start_date = Date.today.beginning_of_week(start_day = :sunday).change({hour: 0, min: 0, sec: 0})
				end_date = Date.today.end_of_week(start_day = :sunday).change({hour: 23, min: 59, sec: 59})
			when 'month'
				start_date = Date.today.beginning_of_month.change({hour: 0, min: 0, sec: 0})
				end_date = Date.today.end_of_month.change({hour: 23, min: 59, sec: 59})
		end

		events = events.where(:data_inicio.lte => end_date, :data_fim.gte => start_date)
		
		render json: events
	end
end