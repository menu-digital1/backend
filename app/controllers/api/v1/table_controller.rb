class Api::V1::TableController < ActionController::Base
  skip_before_action :verify_authenticity_token

  def link
    table = Table.where(numero: params[:number]).first
    #byebug
    if table.blank?
      render json: {
               error: true,
               message:
                 'Mesa não encontrada. Verifique a digitação e tente novamente.'
             }
    elsif table.status.invalid?
      render json: { error: true, message: 'Mesa inválida' }
    elsif table.status.used?
      render json: {
               error: true, message: 'Esta mesa já está associada a um tablet'
             }
    else
      table.status = :used
      if table.save
        render json: { error: false }
      else
        render json: {
                 error: true,
                 message: 'Erro ao vincular mesa. Por favor tente novamente.'
               }
      end
    end
  end

  def unlink
    table = Table.where(numero: params[:number]).first
    #byebug
    if table.blank?
      render json: {
               error: true,
               message:
                 'Mesa não encontrada. Verifique a digitação e tente novamente.'
             }
    elsif table.status.invalid?
      render json: { error: true, message: 'Mesa inválida' }
      # elsif table.status.open?
      #     render json: { error: true, message: 'Esta mesa já está aberta' }
    else
      table.status = :open

      if table.save
        render json: { error: false }
      else
        render json: {
                 error: true,
                 message: 'Erro ao desvincular mesa. Por favor tente novamente.'
               }
      end
    end
  end
end
