class Api::V1::ComandasController < ActionController::Base
  skip_before_action :verify_authenticity_token

  def check_comanda
    error = false
    change_table = false
    message = ''

    comanda = Comanda.where(codigo: params[:code]).first

    if !comanda
      error = true
      message = 'Comanda não existe'
    else
      if comanda.status.invalid?
        error = true
        message = 'Comanda inválida'
      else
        # if comanda.status.waiting_payment?
        #     error = true
        #     message = 'Essa Comanda possui um pagamento em aberto, por favor, verifique com o garçom'
        # else
        if comanda.table
          change_table = true if comanda.table.numero != params[:table].to_i
        end
        # end
      end
    end

    render json: { error: error, change_table: change_table, message: message }
  end

  def link_comanda
    comanda = Comanda.where(codigo: params[:code]).first

    table = Table.where(numero: params[:table]).first

    comanda.table_id = table.id
    comanda.status = :used

    comanda.save validate: false
  end

  def unlink_comanda
    comanda = Comanda.where(codigo: params[:code]).first

    comanda.unset(:table_id)
    comanda.status = :open

    comanda.save validate: false
  end

  def clear_comandas
    table = Table.where(numero: params[:table]).first
    comanda = Comanda.where(table_id: table.id)

    comanda.each do |c|
      c.unset(:table_id)
      c.status = :open

      c.save validate: false
    end
  end

  def get_orders
    comanda = Comanda.where(numero: params[:commandaNumber])
    return comanda.to_json
  end
end
