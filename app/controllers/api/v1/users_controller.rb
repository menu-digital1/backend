class Api::V1::UsersController < ActionController::Base
  skip_before_action :verify_authenticity_token

  def authenticate
    user = User.where(email: params[:email])

    if user.exists?
      if user.first.valid_password?(params[:password])
        if can_change_settings?(user.first)
          render json: { error: false }
        else
          render json: {
                   error: true, message: 'Este usuário não possui permissão'
                 }
        end
      else
        render json: { error: true, message: 'Senha inválida' }
      end
    else
      render json: { error: true, message: 'Usuário inválido' }
    end
  end

  def can_change_settings?(user)
    user.role.admin?
  end
end
