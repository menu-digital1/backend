class Api::V1::CardapioController < ActionController::Base
  require 'pusher'

  skip_before_action :verify_authenticity_token

  def index
    productIds = Produto.where(habilitado: true).only(:id).map(&:id)
    subIds = Produto.where(habilitado: true).only(:subcategorias_id).map(&:subcategorias_id)
    catIds = Subcategoria.where(habilitado: true, :id.in => subIds, :produto_id.ne => []).only(:categoria_id).map(&:categoria_id)
    categorias = Categoria.where(habilitado: true, :id.in => catIds).order_by(posicao: :asc)

    cardapio = []
    categorias.each do |categoria|
      item = {
        id: categoria.id,
        nome: categoria.nome,
        subcategoria: categoria.subcategorias.where(:id.in => subIds).order_by(posicao: :asc).only(:id, :nome)
      }
      
      cardapio.push(item)
    end

    render json: cardapio
  end

  def fetch
    $request = request

    result = []
    Produto.where(subcategorias_id: params[:subcategorias_id], habilitado: true).order(posicao: :asc).each do |p|
      product = {
        _id: p.id,
        nome: p.nome,
        descricao: p.descricao,
        valor_final: p.valor_final,
        full_imagem: p.full_imagem,
        full_gif: p.full_gif,
        tipo: p.tipo,
        variantes: []
      }

      p.variantes.each do |v|
        if v.habilitado == true
          variantes = {
            _id: v.id,
            nome: v.nome,
            tipo: v.tipo,
            maxSelect: v.maxSelect,
            itens_variante: []
          }

          v.itens_variante.each do |i|
            if i.habilitado == true
              variantes[:itens_variante] <<
                { _id: i.id, nome: i.nome, valor_adicional: i.valor_adicional }
            end
          end

          product[:variantes] << variantes
        end
      end

      result << product
    end

    render json: result
  end

  def call_waiter
    Pusher.trigger('orders-channel', 'call-waiter', { table: params[:table] })

    call_waiter = CallWaiter.new
    call_waiter.table = params[:table]
    call_waiter.save

    head :no_content
  end
end
