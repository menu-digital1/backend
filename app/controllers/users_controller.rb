class UsersController < ApplicationController
  skip_before_action :authenticate_user!, only: %i[save_first_user]
  before_action :set_user, only: %i[show edit update destroy lock unlock]

  def save_first_user
    user = User.new(user_params)

    user.role = :admin

    redirect_to root_path if sign_in user if user.save validate: false
  end

  def profile
    @page_title = 'Perfil'
    @user = current_user
  end

  def save_profile
    user = current_user

    user.update user_params

    if not params['user']['new_password'].blank?
      user.password = params['user']['new_password']
      user.save validate: false
    end

    redirect_to users_profile_path, notice: 'Perfil salvo com sucesso'
  end

  def index
    @page_title = 'Usuários'
    @users = User.all
  end

  def create
    # byebug

    @user = User.new user_params

    # @user.role = user_params2["role"].to_i

    # respond_to do |format|
    #     if user.save validate: false
    #         #redirect_to usuarios_path
    #         format.html { redirect_to user, notice: 'Usuario cadastrado com sucesso' }
    #         format.json { head :no_content }
    #     end
    # end

    @user.save validate: false

    redirect_to users_path, notice: 'Usuário salvo com sucesso'
  end

  def update
    @user.update user_params

    # @user.role = user_params2["role"].to_i

    @user.save validate: false

    redirect_to users_path, notice: 'Usuário salvo com sucesso'
  end

  def new
    @page_title = 'Usuários <small>Novo</small>'
    @user = User.new
  end

  def edit
    @page_title = 'Usuários <small>Editar</small>'
  end

  def lock
    @user.locked = true
    @user.save validate: false

    redirect_to users_path, notice: 'Usuário bloqueado com sucesso'
  end

  def unlock
    @user.locked = false
    @user.save validate: false

    redirect_to users_path, notice: 'Usuário desbloqueado com sucesso'
  end

  def check_email
    email = params['user']['email']

    if params['_action'] == 'update'
      user = User.find(params['_id'])

      if user.email == email
        render json: true
      else
        render json: User.where(email: email).first.nil?
      end
    end

    if params['_action'] == 'insert'
      render json: User.where(email: email).first.nil?
    end
  end

  def check_password
    render json:
             User.find(params['_id']).valid_password?(
               params['user']['old_password']
             )
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html do
        redirect_to users_path, notice: 'Usuario removido com sucesso'
      end
      format.json { head :no_content }
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit :id, :nome, :email, :password, :role
  end
end
