class CategoriasController < ApplicationController
  before_action :set_categoria, only: %i[show edit update destroy]

  # GET /categorias
  # GET /categorias.json
  def index
    @page_title = 'Categorias'
    @categorias = Categoria.order_by(posicao: :asc)
  end

  # GET /categorias/1
  # GET /categorias/1.json
  def show; end

  # GET /categorias/new
  def new
    @page_title = 'Categoria <small>Nova</small>'
    @categoria = Categoria.new
  end

  # GET /categorias/1/edit
  def edit
    @page_title = 'Categoria <small>Editar</small>'
  end

  # POST /categorias
  # POST /categorias.json
  def create
    @categoria = Categoria.new(categoria_params)

    respond_to do |format|
      if @categoria.save
        format.html do
          redirect_to categorias_url, notice: 'Categoria criada com sucesso.'
        end
        format.json { render :index, status: :created, location: @categoria }
      else
        format.html { render :new }
        format.json do
          render json: @categoria.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # PATCH/PUT /categorias/1
  # PATCH/PUT /categorias/1.json
  def update
    respond_to do |format|
      if @categoria.update(categoria_params)
        format.html do
          redirect_to categorias_url, notice: 'Categoria alterada com sucesso.'
        end
        format.json { render :index, status: :ok, location: @categoria }
      else
        format.html { render :edit }
        format.json do
          render json: @categoria.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # DELETE /categorias/1
  # DELETE /categorias/1.json
  def destroy
    @categoria.destroy
    respond_to do |format|
      format.html do
        redirect_to categorias_url,
                    notice: 'Categoria was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  #Habilitar e Desabilitar categoria
  def toggle_habilitado
    @categoria = Categoria.find(params[:categoria_id])
    @categoria.toggle_habilitado

    if @categoria.save
      head :ok
    else
      head :ok, status: :unprocessable_entity
    end
  end

  #Ajustar posicao da categoria
  def update_posicao
    @categoria = Categoria.find(params[:categoria_id])
    @categoria.posicao = params[:posicao]

    if @categoria.save
      head :ok
    else
      head :ok, status: :unprocessable_entity
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_categoria
    @categoria = Categoria.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def categoria_params
    params.require(:categoria).permit(:nome)
  end
end
