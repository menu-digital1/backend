class PrintLogsController < ApplicationController
  skip_before_action :verify_authenticity_token
  skip_before_action :authenticate_user!

  require 'pusher'

  def create
    log = PrintLog.new

    if params[:table]
      log.descricao = "A mesa #{sprintf '%02d', params[:table]} foi fechada"
      log.table = params[:table]
    else
      first_comanda = nil
      params[:orders].each do |comanda, o|
        first_comanda = comanda
        break
      end

      log.descricao = "A comanda #{sprintf '%02d', first_comanda} foi fechada"
    end

    log.status = params[:status]
    log.produtos = params[:orders].to_unsafe_h
    log.company_data = params[:company_data].to_unsafe_h

    log.save validate: false

    Pusher.trigger('log-channel', 'update-logs', { message: true })
  end

  def fetch
    render json: PrintLog.order_by(created_at: :desc).limit(30)
  end
end
