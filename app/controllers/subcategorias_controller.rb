class SubcategoriasController < ApplicationController
  before_action :set_subcategoria, only: %i[show edit update destroy]
  before_action :set_categoria, only: %i[index new edit update create destroy]

  # GET /categorias
  # GET /categorias.json
  def index
    @page_title = @categoria.nome + ' <small>Subcategorias</small>'
    @subcategorias = @categoria.subcategorias.all
  end

  # GET /categorias/1
  # GET /categorias/1.json
  def show; end

  # GET /categorias/new
  def new
    @page_title = @categoria.nome + ' <small>Nova Subcategoria</small>'
    @subcategoria = Subcategoria.new
  end

  # GET /categorias/1/edit
  def edit
    @page_title = @categoria.nome + ' <small>Editar Subcategoria</small>'
  end

  # POST /categorias
  # POST /categorias.json
  def create
    @subcategoria = Subcategoria.new(categoria_params)

    respond_to do |format|
      if @subcategoria.save
        format.html do
          redirect_to categoria_subcategorias_path(@categoria),
                      notice: 'Subcategoria criada com sucesso.'
        end
        format.json { render :index, status: :created, location: @subcategoria }
      else
        format.html { render :new }
        format.json do
          render json: @subcategoria.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # PATCH/PUT /categorias/1
  # PATCH/PUT /categorias/1.json
  def update
    respond_to do |format|
      if @subcategoria.update(categoria_params)
        format.html do
          redirect_to categoria_subcategorias_path(@categoria),
                      notice: 'Subcategoria alterada com sucesso.'
        end
        format.json { render :index, status: :ok, location: @subcategoria }
      else
        format.html { render :edit }
        format.json do
          render json: @subcategoria.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # DELETE /categorias/1
  # DELETE /categorias/1.json
  def destroy
    @subcategoria.destroy
    respond_to do |format|
      format.html do
        redirect_to categoria_subcategorias_path(@categoria),
                    notice: 'Subcategoria was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  #Habilitar e Desabilitar categoria
  def toggle_habilitado
    @subcategoria = Subcategoria.find(params[:subcategoria_id])
    @subcategoria.toggle_habilitado

    if @subcategoria.save
      head :ok
    else
      head :ok, status: :unprocessable_entity
    end
  end

  #Ajustar posicao da subcategoria
  def update_posicao
    @subcategoria =
      Categoria.find(params[:categoria_id]).subcategorias.find(
        params[:subcategoria_id]
      )
    @subcategoria.posicao = params[:posicao]

    if @subcategoria.save
      head :ok
    else
      head :ok, status: :unprocessable_entity
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_categoria
    @categoria = Categoria.find(params[:categoria_id])
  end

  def set_subcategoria
    @subcategoria = Subcategoria.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def categoria_params
    params.require(:subcategoria).permit(:nome, :categoria_id)
  end
end
