class HomeController < ApplicationController
    skip_before_action :verify_authenticity_token, only: [:waiter_calls]
    skip_before_action :authenticate_user!, only: [:waiter_calls]

    def index
        if current_user.role.kitchen? or current_user.role.balcony?
            redirect_to balcony_path
        end

        @page_title = 'Bem-vindo(a), ' + current_user.nome

        @active_tables = Table.or(status: :used).or(status: :waiting_payment).count
        @active_comandas = Comanda.or(status: :used).or(status: :waiting_payment).or(status: :waiting_payment_no_comission).count
    end

    def waiter_calls
        waiter_calls = CallWaiter.where(
                :created_at.gte => params[:start_date], 
                :created_at.lte => params[:end_date])
            .group_by(&:table)

        render json: waiter_calls
    end
end