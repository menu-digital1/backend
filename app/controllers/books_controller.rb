class BooksController < ApplicationController
  require 'fileutils'

  before_action :set_book, only: %i[edit update destroy]

  def index
    @page_title = 'Livros'

    @books = Book.order(titulo: :asc).page(params[:page])

    if !params[:q].blank?
      categories_ids = CategoryBook.where(nome: /.*#{params[:q]}.*/i).pluck(:id)
      @books =
        @books.any_of(
          { titulo: /.*#{params[:q]}.*/i },
          { autor: /.*#{params[:q]}.*/i },
          { resumo: /.*#{params[:q]}.*/i },
          { :category_book_id.in => categories_ids }
        )
    end
  end

  def new
    @page_title = 'Livros <small>Novo</small>'

    @categories = CategoryBook.all
    @book = Book.new
  end

  def edit
    @page_title = 'Livros <small>Editar</small>'

    @categories = CategoryBook.all
  end

  def create
    @book = Book.new book_params

    # @book.category_book = CategoryBook.find(params[:book][:category_book])

    @book.capa = params[:book][:capa_file]

    @book.save validate: false

    redirect_to books_path, notice: 'Livro salvo com sucesso'
  end

  def update
    @book.update book_params

    @book.capa = params[:book][:capa_file] if !params[:book][:capa_file].blank?

    @book.save validate: false

    redirect_to books_path, notice: 'Livro salvo com sucesso'
  end

  def add_category
    category = CategoryBook.new params.permit(:nome)

    category.save

    render json: category
  end

  def destroy
    @book.destroy

    redirect_to books_path, notice: 'Livro removido com sucesso'
  end

  private

  def set_book
    @book = Book.find params[:id]
  end

  def book_params
    params.require(:book).permit :titulo, :autor, :resumo, :category_book_id
  end
end
