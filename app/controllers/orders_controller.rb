class OrdersController < ApplicationController
    before_action :set_order, only: [:destroy]
    skip_before_action :verify_authenticity_token, only: [:orders_by_date]

    def index
        @page_title = 'Pedidos'
        @orders = Order.all
    end

    def destroy
        @order.destroy
        
        redirect_to orders_path, notice: 'Pedido excluído com sucesso.'
    end

    def orders_by_date
        orders = Order.where(:created_at.gte => params[:start_date], :created_at.lte => params[:end_date], status: :closed)

        dates = (params[:start_date].to_date..params[:end_date].to_date).map do |d|
            [d.strftime('%Y-%m-%d'), 0]
        end
        dates = Hash[dates]

        orders.group_by{|o| o.created_at.strftime('%Y-%m-%d')}.each do |date, orders|
            dates[date] = orders.sum {|o| o.order_total}
        end

        render json: dates
    end

    private
    def set_order
        @order = Order.find(params[:id])
    end
end