$('#company_data_cnpj').mask('99.999.999/9999-99');
$('#company_data_cep').mask('99999-999');

$('#company_data_cep').blur(function() {
    var cep = $(this).val().replace('-', '');

    $.get('https://handover.agenciamaior.com.br/api-cep/v1/cep?cep=' + cep).then(response => {
        $("#company_data_endereco").val(response.tipo_logradouro + ' ' + response.logradouro);
        $("#company_data_bairro").val(response.bairro);
        $("#company_data_cidade").val(response.cidade);
        $("#company_data_uf").val(response.uf);
    });
});

$('#company-data-form').validate({
    rules: {
        'company_data[nome]': 'required',
        'company_data[cnpj]': {
            required: true,
            minlength: 18,
        },
        'company_data[cep]': {
            required: true,
            minlength: 9,
        },
        'company_data[endereco]': 'required',
        'company_data[numero]': 'required',
        'company_data[bairro]': 'required',
        'company_data[cidade]': 'required',
        'company_data[uf]': 'required',
    },
    messages: {
        'company_data[cnpj]': {
            minlength: 'CNPJ inválido',
        },
        'company_data[cep]': {
            minlength: 'CEP inválido',
        },
    }
});