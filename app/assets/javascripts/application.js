// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require js-routes
//= require rails-ujs
//= require popper
//= require bootstrap-sprockets
//= require jquery.validate 
//= require jquery.validate.additional-methods
//= require datatables/jquery.dataTables
//= require datatables/dataTables.bootstrap4
//= require ./vendor/adminlte
//= require ./vendor/jquery.mask
//= require ./vendor/moment.min
//= require ./vendor/daterangepicker
//= require select2
//= require moment
//= require chosen-jquery
//= require maskmoney
//= require jquery.inputmask
//= require ./i18n/pt-br
//= require chartkick
//= require Chart.bundle
//= require summernote/summernote-bs4.min
//= require_tree .

$('.datepicker').datepicker();
$(function() {
    $('input[name="datetimes"]').daterangepicker({
      timePicker: true,
      startDate: moment().startOf('hour'),
      endDate: moment().startOf('hour').add(32, 'hour'),
      locale: {
        format: 'M/DD hh:mm A'
      }
    });
  });

$.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param * 1024 * 1024)
}, $.validator.messages.filesize);

$.validator.addMethod("greaterEqThan", function (value, element, param) {
    var $otherElement = $(param);
    return parseInt(value, 10) >= parseInt($otherElement.val(), 10);
}, $.validator.messages.period);

$.validator.addMethod("dateBR", function (value, element) {
    return this.optional(element) || moment(value, 'DD/MM/YYYY').isValid();
}, $.validator.messages.date);

$.validator.addMethod('period', function (value, element, params) {
    var startDate = moment($('input[name="' + params[0] + '"]').val(), 'DD/MM/YYYY');
    var endDate = moment(value, 'DD/MM/YYYY');

    return this.optional(element) || startDate <= endDate;
}, $.validator.messages.period);

function toggleHabilitado(self, url) {
    $.ajax({
        url: url,
        type: 'POST',
        success: function(data) {
            self.prop("checked", !self.prop("checked"));
        },
        error: function() {
            alert("Erro ao atualizar, por favor tente novamente.");
        }
    });
}

$.validator.setDefaults({
    highlight: function (element, errorClass, validClass) {
        if ($(element).hasClass("select-2")) {
            $(element).siblings('.select2').addClass(errorClass);
        } else {
            $(element).addClass(errorClass);
        }
    },
    unhighlight: function (element, errorClass, validClass) {
        if ($(element).hasClass("select-2")) {
            $(element).next('.select2').removeClass(errorClass);
        } else {
            $(element).removeClass(errorClass);
        }
    },
    errorPlacement: function(error, element) {
        if (element.hasClass('select-2')) {
            error.insertAfter(element.next('.select2'));
        } else {
            error.insertAfter(element);
        }
    },
});