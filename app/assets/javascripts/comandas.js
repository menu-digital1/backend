$('#comandas-list').DataTable();

function toggleActiveComanda(id) {
    $.post('/comandas/active', { id });
}

$('#comanda_table_id').select2({
    theme: "bootstrap4",
});

$('#comanda-new-form').validate({
    rules: {
        'start': {
            'required': true,
            'digits': true,
        },
        'end': {
            'required': true,
            'digits': true,
            'greaterEqThan': '#start',
        },
    },
    messages: {
        'end': {
            'greaterEqThan': 'Deve ser maior ou igual ao Início',
        },
    }
});

$('#comanda-edit-form').validate({
    rules: {
        'comanda[codigo]': {
            'required': true,
            'digits': true,
        },
        'comanda[status]': 'required',
    },
});