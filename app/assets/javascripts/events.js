$('.editor').summernote({
	lang: 'pt-BR',
	height: 152,
	toolbar: [
		['style', ['bold', 'italic', 'underline', 'clear']],
		['font', ['strikethrough', 'superscript', 'subscript']],
		['para', ['ul', 'ol', 'paragraph']],
		['undoredo', ['undo', 'redo']]
	]
});

$('#event-form').validate({
	rules: {
		'event[nome]': 'required',
		'event[descricao]': 'required',
		'event[imagem]': {
			required: () => !$('#event_id').val(),
			extension: 'jpg|png|jpeg',
			filesize: 2
		},
		'event[data_inicio]': {
			required: true,
			dateBR: true
		},
		'event[data_fim]': {
			required: true,
			dateBR: true,
			period: ['event[data_inicio]']
		}
	},
	messages: {
		'event[imagem]': {
			extension: 'Somente arquivos de imagem (PNG ou JPG)'
		}
	}
});
