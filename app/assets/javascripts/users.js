$('#profile-form').validate({
    rules: {
        'user[nome]': 'required',
        'user[email]': {
            'required': true,
            'email': true,
            'remote': {
                url: '/users/check-email',
                type: 'post',
                data: {
                    _action: 'update',
                    _id: $('#user_id').val(),
                }
            },
        },
        'user[old_password]': {
            'remote': {
                depends: () => $('#user_old_password').val() !== '',
                param: {
                    url: '/users/check-password',
                    type: 'post',
                    data: {
                        _id: $('#user_id').val(),
                    }
                }
            },
        },
        'user[new_password]': {
            'required': () => $('#user_old_password').val() !== '',
        },
        'user[confirm_new_password]': {
            'required': () => $('#user_old_password').val() !== '' && $('#user_new_password').val() !== '',
            'equalTo': '#user_new_password'
        }
    },
    messages: {
        'user[email]': {
            'remote': 'Este e-mail já está sendo utilizado'
        },
        'user[old_password]': {
            'remote': 'Senha inválida'
        },
        'user[confirm_new_password]': {
            'equalTo': 'Deve ser igual à Nova Senha'
        }
    }
});

$('#user-form').validate({
    rules: {
        'user[nome]': 'required',
        'user[email]': {
            'required': true,
            'email': true,
            'remote': {
                url: '/users/check-email',
                type: 'post',
                data: {
                    _action: $('#insert_mode').val() == 'true' ? 'insert' : 'update',
                    _id: $('#user_id').val(),
                }
            },
        },
        'user[password]': {
            'required': () => $('#insert_mode').val() == 'true'
        },
        'user[confirm_password]': {
            'required': true,
            'equalTo': '#user_password',
        },
    },
    messages: {
        'user[email]': {
            'remote': 'Este e-mail já está sendo utilizado'
        },
        'user[confirm_password]': {
            'equalTo': 'Deve ser igual à Senha',
        },
    }
});

$('#users-list').DataTable();