$('#banner_produto_id').select2({
    theme: "bootstrap4",
});
$('#banner_event_id').select2({
    theme: "bootstrap4",
});

checkBannerAssociation();

$('input[name=banner_association_choice]').change(function() {
    checkBannerAssociation();
});

function checkBannerAssociation() {
    $('#product-select-container').hide();
    $('#event-select-container').hide();

    if ($('#banner_association_choice_product').prop('checked')) {
        $('#product-select-container').fadeIn();
    }

    if ($('#banner_association_choice_event').prop('checked')) {
        $('#event-select-container').fadeIn();
    }
}

$('#banner-form').validate({
    rules: {
        'banner[texto]': 'required',
        'banner[imagem]': {
            'required': () => !$('#banner_id').val(),
            'extension': 'jpg|png|jpeg',
            'filesize': 2,
        },
        'banner[data_inicio]': {
            'dateBR': true,
        },
        'banner[data_fim]': {
            'required': () => !!$('#banner_data_inicio').val(),
            'dateBR': true,
            'period': ['banner[data_inicio]'],
        },
    },
    messages: {
        'banner[imagem]': {
            'extension': 'Somente arquivos de imagem (PNG ou JPG)',
        },
    },
});