$(".sub-categoria-habilitado-toggle").click(function(e) {
    e.preventDefault();

    var self = $(this);
    var subcategoria_id = $(this).attr("id");
    var categoria_id = $(this).data("categoria-id");

    var url = Routes.categoria_subcategoria_toggle_habilitado_path(categoria_id, subcategoria_id);

    toggleHabilitado(self, url);
    //$('button[data-categoria-id='+categoria_id+']').prop("disabled", false);
    $('button[data-subcategoria-id='+subcategoria_id+']').prop('disabled', function(i, v) { return !v; });
    $('.collapse[data-produto-categoria-subcategoria-id='+subcategoria_id+']').collapse('hide')
});

$(".categoria-habilitado-toggle").click(function(e) {
    e.preventDefault();

    var self = $(this);
    var categoria_id = $(this).data("categoria-id");

    var url = Routes.categoria_toggle_habilitado_path(categoria_id);

    toggleHabilitado(self, url);
    //$('button[data-categoria-id='+categoria_id+']').prop("disabled", false);
    $('button[data-categoria-id='+categoria_id+']').prop('disabled', function(i, v) { return !v; });
    $('.collapse[data-produto-categoria-id='+categoria_id+']').collapse('hide')
});

$('#categorias-list').DataTable();

$('#categoria').validate({
    rules: {
        'categoria[nome]': 'required',
        'categoria[imagem]': 'required',
    }
});