$('#tables-list').DataTable();

$('#table-form').validate({
    rules: {
        'table[numero]': {
            'required': true,
            'digits': true,
        },
    },
});