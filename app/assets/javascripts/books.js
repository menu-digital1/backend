$('#book_category_book_id').select2({
    theme: "bootstrap4",
    tags: true,
    escapeMarkup: m => m,
    createTag: params => {
        var term = $.trim(params.term);

        return {
            id: term,
            text: term + ' <small>(Adicionar nova categoria)</small>',
            rawText: term,
            new: true,
        }
    }
}).on("select2:select", function(e) {
    if (e.params.data.new) {
        if (confirm('Tem certeza que deseja adicionar uma nova Categoria?')) {
            $(this).find('[value="'+e.params.data.id+'"]').replaceWith('<option selected value="'+e.params.data.id+'">'+e.params.data.rawText+'</option>');

            $.post('/books/add-category', {nome: e.params.data.rawText}, response => {
                $(this).find('[value="'+e.params.data.id+'"]').replaceWith('<option selected value="'+response._id+'">'+e.params.data.rawText+'</option>');
            });
        }
    }
});

$('#book-form').validate({
    rules: {
        'book[titulo]': 'required',
        'book[autor]': 'required',
        'book[category_book_id]': 'required',
        'book[capa_file]': {
            'required': () => !$('#book_id').val(),
            'extension': 'jpg|png|jpeg',
            'filesize': 2,
        },
        'book[resumo]': 'required',
    },
    messages: {
        'book[capa_file]': {
            'extension': 'Somente arquivos de imagem (PNG ou JPG)',
        },
    },
});