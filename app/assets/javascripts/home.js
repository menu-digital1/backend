// Chartkick.options = {
//     height: "400px"
// }
// $(function() {
//     var start = moment().subtract(7, 'days');
//     var end = moment();

//     function cb(start, end) {
//         $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
//         new Chartkick.LineChart("linechart", "/line_chart_pedidos?start="+start.format('DD/MM/YYYY')+"&end="+end.format('DD/MM/YYYY'), {legend: "bottom", curve: true});
//         new Chartkick.BarChart("callwaiterchart", "/call_waiter?start="+start.format('DD/MM/YYYY')+"&end="+end.format('DD/MM/YYYY'), {legend: "bottom"});
//         //new Chartkick.PieChart("piechart", "/pie_chart_pedidos?start="+start.format('DD/MM/YYYY')+"&end="+end.format('DD/MM/YYYY'), {legend: "right"});
//     }

//     $('#reportrange').daterangepicker({
//         startDate: start,
//         endDate: end,
//         ranges: {
//         'Hoje': [moment(), moment()],
//         'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
//         'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
//         'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
//         'Este Mês': [moment().startOf('month'), moment().endOf('month')],
//         'Último Mês': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
//         },
//         "locale": {
//             "format": "DD/MM/YYYY",
//             "separator": " / ",
//             "applyLabel": "Aplicar",
//             "cancelLabel": "Cancelar",
//             "fromLabel": "De",
//             "toLabel": "Para",
//             "customRangeLabel": "Personalizar",
//             "weekLabel": "S",
//             "daysOfWeek": [
//                 "D",
//                 "S",
//                 "T",
//                 "Q",
//                 "Q",
//                 "S",
//                 "S"
//             ],
//             "monthNames": [
//                 "Janeiro",
//                 "Fevereiro",
//                 "Março",
//                 "Abril",
//                 "Maio",
//                 "Junho",
//                 "Julho",
//                 "Agosto",
//                 "Setembro",
//                 "Outubro",
//                 "Novembro",
//                 "Dezembro"
//             ],
//             "firstDay": 1
//         }
//     }, cb);

//     cb(start, end);
// });