$('#accordion').accordion({
    collapsible: true,
    active: false,
    height: 'fill',
    header: 'h3'
}).on('hide.bs.collapse', function (e) {
    var categoria_id = $(e.target.previousElementSibling).data("categoria-id");
    $('button[data-categoria-id='+categoria_id+'] i').removeClass('fa-minus').addClass('fa-plus');
}).on('show.bs.collapse', function (e) {
    var categoria_id = $(e.target.previousElementSibling).data("categoria-id");
    $('button[data-categoria-id='+categoria_id+'] i').removeClass('fa-plus').addClass('fa-minus');
}).sortable({
    items: '.s_panel',
    update: function(event, ui) {
        console.log(ui);
        var self = $(this);
        var old_position = ui.item.data("start_pos");
        var new_position = ui.item.index();
        var categoria_id = ui.item.data("categoria-id");
        var url = Routes.categoria_update_posicao_path(categoria_id);
        updatePosicao(url, self, old_position, new_position);
    }
});

$('.accordionsubs').accordion({
    collapsible: true,
    active: false,
    height: 'fill',
    header: 'h3'
}).on('hide.bs.collapse', function (e) {
    var subcategoria_id = $(e.target.previousElementSibling).data("subcategoria-id");
    $('button[data-subcategoria-id='+subcategoria_id+'] i').removeClass('fa-minus').addClass('fa-plus');
}).on('show.bs.collapse', function (e) {
    var subcategoria_id = $(e.target.previousElementSibling).data("subcategoria-id");
    $('button[data-subcategoria-id='+subcategoria_id+'] i').removeClass('fa-plus').addClass('fa-minus');
}).sortable({
    items: '.s_panel',
    update: function(event, ui) {
        console.log(ui);
        var self = $(this);
        var old_position = ui.item.data("start_pos");
        var new_position = ui.item.index();
        var categoria_id = ui.item.data("categoria-id");
        var subcategoria_id = ui.item.data("subcategoria-id");
        var url = Routes.categoria_subcategoria_update_posicao_path(categoria_id,subcategoria_id);
        updatePosicao(url, self, old_position, new_position);
    }
});

$('.accordionProduto').accordion({
    collapsible: true,
    active: false,
    height: 'fill',
    header: 'h3'
}).on('hide.bs.collapse', function (e) {
    var produto_id = $(e.target.previousElementSibling).data("produto-id");
    $('button[data-produto-id='+produto_id+'] i').removeClass('fa-minus').addClass('fa-plus');
}).on('show.bs.collapse', function (e) {
    var produto_id = $(e.target.previousElementSibling).data("produto-id");
    $('button[data-produto-id='+produto_id+'] i').removeClass('fa-plus').addClass('fa-minus');
}).sortable({
    items: '.li-produto',
    update: function(event, ui) {
        console.log(ui.item.data("produto-id"));
        var self = $(this);
        var old_position = ui.item.data("start_pos");
        var new_position = ui.item.index();
        var produto_id = ui.item.data("produto-id");
        var url = Routes.produto_update_posicao_path(produto_id);
        updatePosicao(url, self, old_position, new_position);
    }
});

$('.accordionVariante').accordion({
    collapsible: true,
    active: false,
    height: 'fill',
    header: 'h3'
}).on('hide.bs.collapse', function (e) {
    var variante_id = $(e.target.previousElementSibling).data("variante-id");
    $('button[data-variante-id='+variante_id+'] i').removeClass('fa-minus').addClass('fa-plus');
}).on('show.bs.collapse', function (e) {
    var variante_id = $(e.target.previousElementSibling).data("variante-id");
    $('button[data-variante-id='+variante_id+'] i').removeClass('fa-plus').addClass('fa-minus');
})

$('#accordion').on('accordionactivate', function (event, ui) {
    if (ui.newPanel.length) {
        $('#accordion').sortable('disable');
    } else {
        $('#accordion').sortable('enable');
    }
});

function updatePosicao(url, self, old_position, new_position) {
    $.ajax({
        url: url,
        type: 'POST',
        data: { posicao: new_position },
        beforeSend: function() {
            self.sortable("disable");
        },
        complete: function() {
            self.sortable("enable");
        },
        error: function() {
            self.sortable("cancel");
            alert("Erro ao atualizar, por favor tente novamente.");
        }
    });
} 


$('.produtos-list').DataTable();