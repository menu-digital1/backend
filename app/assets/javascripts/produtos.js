$(".produto-habilitado-toggle").click(function(e) {
    e.preventDefault();

    var self = $(this);
    var categoria_id = $(this).data("categoria-id");
    var produto_id = $(this).data("produto-id");

    var url = Routes.produto_toggle_habilitado_path(produto_id);

    toggleHabilitado(self, url);
});

$('#produtos-list').DataTable();

$('#produto_valor_final').maskMoney({
    thousands: '.',
    decimal: ',',
});
$('#produto_valor_promocional').maskMoney({
    thousands: '.',
    decimal: ',',
});

$('#produto-form').validate({
    rules: {
        'produto[nome]': 'required',
        'produto[imagem]': {
            'required': () => !$('#produto_id').val(),
            'extension': 'jpg|png|jpeg',
            'filesize': 2,
        },
        'produto[gif]': {
            'extension': 'gif',
            'filesize': 2,
        },
        'produto[categoria_id]': 'required',
        'produto[descricao]': 'required',
        'produto[valor_final]': 'required',
    },
    messages: {
        'produto[imagem]': {
            'extension': 'Somente arquivos de imagem (JPG ou PNG)',
        },
        'produto[gif]': {
            'extension': 'Somente GIFs',
        },
    }
});