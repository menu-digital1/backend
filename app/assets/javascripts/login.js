$('#first-user-form').validate({
    rules: {
        'user[nome]': 'required',
        'user[email]': {
            'required': true,
            'email': true,
        },
        'user[password]': 'required',
        'user[confirm_password]': {
            'required': true,
            'equalTo': '#user_password',
        },
    },
    messages: {
        'user[confirm_password]': {
            'equalTo': 'Deve ser igual à Senha',
        },
    }
});