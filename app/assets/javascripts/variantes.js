
$(".variante-habilitado-toggle").click(function(e) {
    e.preventDefault();

    var self = $(this);
    var produto_id = $(this).data("produto-id");
    var id = $(this).data("variante-id");

    var url = Routes.produto_variante_toggle_habilitado_path(produto_id, id);

    toggleHabilitado(self, url);
});

$(".itemvariante-habilitado-toggle").click(function(e) {
    e.preventDefault();

    var self = $(this);
    var produto_id = $(this).data("produto-id");
    var variante_id = $(this).data("variante-id");
    var id = $(this).data("itens-variante-id");

    var url = Routes.produto_variante_itens_variante_toggle_habilitado_path(produto_id, variante_id, id);

    toggleHabilitado(self, url);
});

$('#variantes-list').DataTable();
$('#itens-variante-list').DataTable();

//$('.money').mask('000.000.000.000.000,00', {reverse: true});

$('#variante').validate({
    rules: {
        'variante[nome]': 'required',
        'variante[maxSelect]': 'required',
        'variante[tipo]': 'required',
    }
});