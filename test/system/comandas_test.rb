require 'application_system_test_case'

class ComandasTest < ApplicationSystemTestCase
  setup { @comanda = comandas(:one) }

  test 'visiting the index' do
    visit comandas_url
    assert_selector 'h1', text: 'Comandas'
  end

  test 'creating a Comanda' do
    visit comandas_url
    click_on 'New Comanda'

    click_on 'Create Comanda'

    assert_text 'Comanda was successfully created'
    click_on 'Back'
  end

  test 'updating a Comanda' do
    visit comandas_url
    click_on 'Edit', match: :first

    click_on 'Update Comanda'

    assert_text 'Comanda was successfully updated'
    click_on 'Back'
  end

  test 'destroying a Comanda' do
    visit comandas_url
    page.accept_confirm { click_on 'Destroy', match: :first }

    assert_text 'Comanda was successfully destroyed'
  end
end
