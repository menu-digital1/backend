Rails.application.routes.draw do
  resources :banners
  root to: 'home#index'

  #Rodas do Dashboard
  get 'chart_pedidos' => 'dashboard#chart_pedidos'
  get 'line_chart_pedidos' => 'dashboard#line_chart_pedidos'
  get 'pie_chart_pedidos' => 'dashboard#pie_chart_pedidos'
  get 'table_pedido_itens' => 'dashboard#table_pedido_itens'
  get 'historico_pedido_itens' => 'dashboard#historico_pedido_itens'
  get 'table_pedido_clientes' => 'dashboard#table_pedido_clientes'
  get 'resumo_pedidos' => 'dashboard#resumo_pedidos'
  get 'call_waiter' => 'dashboard#call_waiter'

  concern :toggleable do
    post 'toggle_habilitado'
  end

  namespace 'users' do
    post 'first-user', action: 'save_first_user', as: 'save_first_user'
    get 'profile', action: 'profile', as: 'profile'
    patch 'profile', action: 'save_profile', as: 'save_profile'
    post 'check-email', action: 'check_email', as: 'check_email'
    post 'check-password', action: 'check_password', as: 'check_password'
    patch ':id/lock', action: 'lock', as: 'lock'
    patch ':id/unlock', action: 'unlock', as: 'unlock'
  end
  resources :categorias, concerns: :toggleable do
    post 'update_posicao'
    resources :subcategorias, concerns: :toggleable do
      post 'update_posicao'
    end
  end
  namespace 'produtos' do
    post 'add-categoria', action: 'add_categoria', as: 'add_categoria'
    get 'new/:subcategory', action: 'new', as: 'new_produto'
  end

  resources :produtos, concerns: :toggleable do
    post 'update_posicao'
    resources :variantes, concerns: :toggleable do
      resources :itens_variantes, concerns: :toggleable
    end
  end

  resources :users
  devise_for :users, path: ''

  resources :categories_books

  namespace 'books' do
    post 'add-category', action: 'add_category', as: 'add_category'
  end
  resources :books

  namespace 'comandas' do
    post 'active', action: 'toggle_active', as: 'active'
    get 'check', action: 'check_comanda', as: 'check'
    get 'check-comanda/:comanda', action: 'check_comanda_action', as: 'check_comanda'
  end
  resources :comandas

  namespace 'api' do
    namespace 'v1' do
      post 'check-comanda', to: 'comandas#check_comanda', as: 'check_comanda'
      post 'link-comanda', to: 'comandas#link_comanda', as: 'link_comanda'
      post 'unlink-comanda', to: 'comandas#unlink_comanda', as: 'unlink_comanda'
      post 'clear-comandas', to: 'comandas#clear_comandas', as: 'clear_comandas'
      post 'auth', to: 'users#authenticate', as: 'auth'
      get 'books/categories', to: 'books#categories', as: 'books_categories'
      post 'books', to: 'books#fetch', as: 'books'
      get 'banners', to: 'banners#fetch', as: 'banners'

      get 'cardapio', to: 'cardapio#index', as: 'cardapio_index'
      post 'cardapio/produtos/:subcategorias_id',
           to: 'cardapio#fetch', as: 'cardapio_produtos'
      get 'call-waiter/:table', to: 'cardapio#call_waiter', as: 'call-waiter'

      post 'orders', to: 'pedido#orders', as: 'orders'
      post 'pedido', to: 'pedido#create', as: 'pedido'

      #Tables
      get 'table/link/:number', to: 'table#link', as: 'link'
      get 'table/unlink/:number', to: 'table#unlink', as: 'unlink'

      #Checkout
      post 'checkout/ordersby_comanda',
           to: 'checkout#ordersby_comanda', as: 'ordersby_comanda'
      post 'checkout/pedir_conta', to: 'checkout#pedir_conta', as: 'pedir_conta'
      post 'checkout/print', to: 'checkout#print', as: 'print'

      post 'events', to: 'events#fetch'
    end
  end

  scope 'cardapio' do
    scope 'categories' do
      get '', to: 'cardapio#fetch_categories'
      post '', to: 'cardapio#add_category'
      put '', to: 'cardapio#update_category'
      delete ':id', to: 'cardapio#delete_category'
      post 'reorder', to: 'cardapio#reorder_categories'
      post 'toggle', to: 'cardapio#toggle_category'
    end

    scope 'subcategories' do
      post '', to: 'cardapio#add_subcategory'
      put '', to: 'cardapio#update_subcategory'
      delete ':id', to: 'cardapio#delete_subcategory'
      post 'reorder', to: 'cardapio#reorder_subcategories'
      post 'toggle', to: 'cardapio#toggle_subcategory'
    end

    scope 'products' do
      get ':subcategory', to: 'cardapio#fetch_produtos'
      post 'reorder', to: 'cardapio#reorder_products'
      post 'toggle', to: 'cardapio#toggle_product'
    end

    scope 'variants' do
      post 'reorder', to: 'cardapio#reorder_variants'
      post 'toggle', to: 'cardapio#toggle_variant'
      post '', to: 'cardapio#create_variant'
      put '', to: 'cardapio#update_variant'
      delete ':product/:id', to: 'cardapio#delete_variant'
    end

    scope 'variants_items' do
      post 'reorder', to: 'cardapio#reorder_variants_items'
      post 'toggle', to: 'cardapio#toggle_variant_item'
      post '', to: 'cardapio#create_variant_item'
      put '', to: 'cardapio#update_variant_item'
      delete ':product/:variant/:id', to: 'cardapio#delete_variant_item'
    end
  end
  resources :cardapio
  resources :tables
  resources :events

  scope 'orders' do
    post 'by-date', to: 'orders#orders_by_date', as: 'orders_by_date'
  end
  resources :orders

  scope 'balcony' do
    get '', to: 'balcony#index', as: 'balcony'
    get 'orders', to: 'balcony#fetch_orders', as: 'balcony_orders'
    post 'order', to: 'balcony#update_order', as: 'update_order'
    get 'finalized', to: 'balcony#finalized', as: 'balcony_finalized'
    get 'fetch-finalized', to: 'balcony#fetch_finalized', as: 'fetch_finalized'
    get 'checkout', to: 'balcony#checkout', as: 'balcony_checkout'
    get 'fetch-checkout', to: 'balcony#fetch_checkout', as: 'fetch_checkout'
    get 'fetch-checkout-waiting',
        to: 'balcony#fetch_checkout_waiting', as: 'fetch_checkout_waiting'
    post 'close-table', to: 'balcony#close_table', as: 'close_table'
    post 'close-comanda', to: 'balcony#close_comanda', as: 'close_comanda'
    post 'print', to: 'balcony#print', as: 'print'
    put 'products-update', to: 'balcony#products_update'
    delete 'order-product/:order/:product',
           to: 'balcony#delete_order_product', as: 'delete_order_product'
  end

  scope 'print_logs' do
    get '', to: 'print_logs#fetch'
    post '', to: 'print_logs#create'
  end

  resources :company_data

  post 'waiter-calls', to: 'home#waiter_calls'
end
